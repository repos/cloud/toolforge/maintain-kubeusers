# Automate the process of generating user credentials for Toolforge Kubernetes

 - Get a list of all the users from LDAP
 - Get a list of namespaces/configmaps in k8s for each toolforge user
 - Do a diff, find new users and users with deleted configmaps
 - For each new user or removed configmap:
    - Create new namespace (only for a new user)
    - generate a CSR (including the right group for RBAC)
    - Validate and approve the CSR
    - Drop the .kube/config file in the tool directory
    - Annotate the namespace with configmap

## Deploying in toolsbeta and tools
This project uses the [standard workflow](https://wikitech.wikimedia.org/wiki/Wikimedia_Cloud_Services_team/EnhancementProposals/Toolforge_Kubernetes_component_workflow_improvements):
1. Build the container image using the
    `wmcs.toolforge.k8s.component.build` cookbook.
2. Update the file for the project you're updating in `deployment/values`.
   Commit those changes to the repository and get it merged in Gerrit.
3. Use the `wmcs.toolforge.k8s.component.deploy` cookbook to deploy the updated
   image to the cluster.

## Deploying locally

Follow these steps:

1. Have a local kubernetes deployment for Toolforge (you can use [lima-kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo/))
2. Build the Docker image locally and load it into the local kubernetes deployment

```shell-session
# if using lima-kilo (kind)
$ docker build -f .pipeline/blubber.yaml --target image -t maintain-kubeusers:dev . && kind load docker-image maintain-kubeusers:dev -n toolforge
```
3. Run the deploy script
```shell-session
$ ./deploy.sh local
```

## Running tests

Tests are run using [tox](https://tox.readthedocs.io/en/latest/), normally,
and are built on [pytest](https://pytest.org/en/latest/). As such, to run
tests, install tox by your favorite method and run the `tox` command at the
top level of this folder.

### Updating the VCR cassettes

Tests work anywhere because they use recorded mocks of the network
interactions with a Kubernetes API server (usually an instance of
[minikube](https://github.com/kubernetes/minikube)). These are recorded using
[vcrpy](https://github.com/kevin1024/vcrpy), which is integrated using
pytest-vcrpy, which helps vcrpy speak pytest (using the cassettes as fixtures,
etc.).

You will have to update the cassettes for tests to pass any time you change
interactions with the Kubernetes API in this application. It is not as
convenient as a single command, unfortunately, because it requires an LDAP
system setup (with an RFC that is no longer valid enabled because that's how
WMCS LDAP is set up) and a properly spun up
[lima-kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo/)
testing set up.

The steps are below:

1. Start a local Toolforge cluster using [lima-kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo/).
2. Run the helper script:
```shell-session
user@laptop:~/git/maintain-kubeusers$ helpers/lima-kilo-vcr-recording.sh
```
3. Include the new tests VCR cassettes in your merge request.

### Doing development with a "real" LDAP environment

This should not be needed in most cases, but if you require it, [lima-kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo/)
and [foxtrot-ldap](https://gitlab.wikimedia.org/repos/cloud/toolforge/foxtrot-ldap) are your friends.

TODO: this section should be expanded with more concrete information and examples.
