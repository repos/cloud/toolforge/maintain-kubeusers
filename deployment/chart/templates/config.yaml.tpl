apiVersion: v1
kind: ConfigMap
metadata:
  name: maintain-kubeusers-config
data:
  quotas.yaml: |
    default:
{{ .Values.quotas.default | toYaml | indent 6 }}
{{ if not (.Values.quotas.overrides | empty) }}
    overrides:
{{ .Values.quotas.overrides | toYaml | indent 6 }}
{{ end }}
