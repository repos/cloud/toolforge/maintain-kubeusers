apiVersion: apps/v1
kind: Deployment
metadata:
  name: maintain-kubeusers
  labels:
    app: maintain-kubeusers
spec:
  replicas: 1
  # prevent 2 pods from running in parallel when upgrading
  # this prevents race conditions in which one maintain-kubeuser pod is doing something
  # and the next pod is trying to redo/undo/whatever that very same thing
  # see https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#recreate-deployment
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: maintain-kubeusers
  template:
    metadata:
      labels:
        app: maintain-kubeusers
    spec:
      serviceAccountName: user-maintainer
      nodeSelector:
        kubernetes.wmcloud.org/nfs-mounted: "true"
      volumes:
      - name: my-host-project
        hostPath:
          path: /data/project
          type: Directory
{{ if .Values.mountHome }}
      - name: my-host-home
        hostPath:
          path: /home
          type: Directory
{{ end }}
      - name: my-host-etc-ldap
        hostPath:
          path: /etc/ldap.yaml
          type: File
      - name: maintain-kubeusers-config
        configMap:
          name: maintain-kubeusers-config
      containers:
      - name: maintain-kubeusers
        image: "{{ .Values.image.name }}:{{ .Values.image.tag }}"
        imagePullPolicy: "{{ .Values.image.pullPolicy }}"
        command: {{ .Values.image.command | toJson }}
        args: {{ .Values.image.args | toJson }}
{{ if .Values.runAsRoot }}
        # needed to be able to create tools directories in the shared NFS
        securityContext:
          runAsUser: 0
          allowPrivilegeEscalation: false
{{ end }}
        volumeMounts:
        - mountPath: /data/project
          name: my-host-project
{{ if .Values.mountHome }}
        - mountPath: /home
          name: my-host-home
{{ end }}
        - mountPath: /etc/ldap.yaml
          name: my-host-etc-ldap
        - mountPath: /etc/maintain-kubeusers
          name: maintain-kubeusers-config
{{ if .Values.enableLivelinessProbe }}
        startupProbe:
          exec:
            command:
            - find
            - /tmp/run.check
            - -mmin
            - '+5'
          # start looking for the file after this many seconds of the container being created
          initialDelaySeconds: 10
          # and keep cheking every this many seconds
          periodSeconds: 10
          # allow up to this many failed checks (so max allowed startup time is 10 + 10 * 6 = 70 seconds)
          failureThreshold: 6

        livenessProbe:
          exec:
            command:
            - find
            - /tmp/run.check
            - -mmin
            - '+5'
            - -exec
            - rm
            - /tmp/run.check
            - ;
          # once the startupProbe succeeded, check for liveness every this many seconds
          periodSeconds: 30
          # allow up to this many failed checks (so max allowed dead time is 30 * 5 = 300 seconds)
          # this is a bit high on purpose, to give some room for some operations to take longer
          failureThreshold: 10
{{ end }}
        ports:
          - containerPort: 9000
            name: metrics
            protocol: TCP
