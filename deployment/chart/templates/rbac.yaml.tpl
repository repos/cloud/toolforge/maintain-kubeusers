# role for toolforge tools
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: tools-user
rules:
  - apiGroups:
    - ""
    resources:
    - bindings
    - events
    - limitranges
    - namespaces
    - namespaces/status
    - pods/log
    - pods/status
    - resourcequotas
    - resourcequotas/status

    # ReplicationControllers are the predecessor of Deployments, and not
    # used in Toolforge. For that reason we do not grant tools the ability to
    # create or otherwise manage them. However we grant the ability to list
    # them, or otherwise `kubectl get all` prints an error message at least in
    # 1.21 and 1.22.
    - replicationcontrollers
    verbs:
    - get
    - list
    - watch
  - apiGroups:
    - ""
    resources:
    - configmaps
    - endpoints
    - pods
    - pods/attach
    - pods/exec
    - pods/portforward
    - pods/proxy
    - secrets
    - services
    - services/proxy
    verbs:
    - get
    - list
    - watch
    - create
    - delete
    - deletecollection
    - patch
    - update
  - apiGroups:
    - apps
    resources:
    - controllerrevisions
    - daemonsets
    verbs:
    - get
    - list
    - watch
  - apiGroups:
    - apps
    resources:
    - deployments
    - deployments/rollback
    - deployments/scale
    - replicasets
    - replicasets/scale
    - statefulsets
    - statefulsets/scale
    verbs:
    - get
    - list
    - watch
    - create
    - delete
    - deletecollection
    - patch
    - update
  - apiGroups:
    - autoscaling
    resources:
    - horizontalpodautoscalers
    verbs:
    - get
    - list
    - watch
  - apiGroups:
    - batch
    resources:
    - cronjobs
    - jobs
    verbs:
    - get
    - list
    - watch
    - create
    - delete
    - deletecollection
    - patch
    - update
  - apiGroups:
    - batch
    resources:
    - cronjobs/status
    - jobs/status
    verbs:
    - get
  - apiGroups:
    - networking.k8s.io
    resources:
    - ingresses
    - networkpolicies
    verbs:
    - get
    - list
    - watch
    - create
    - delete
    - deletecollection
    - patch
    - update
  - apiGroups:
    - policy
    resources:
    - poddisruptionbudgets
    verbs:
    - get
    - list
    - watch
  - apiGroups:
    - metrics.k8s.io
    resources:
    - pods
    verbs:
    - get
    - list
    - watch
---
# role for kubernetes project admins
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: k8s-admin
rules:
  - apiGroups:
    - ""
    resources:
    - users
    - groups
    - serviceaccounts
    verbs:
    - impersonate

---
# user-maintainer role for the service account this runs under
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: user-maintainer
rules:
- apiGroups:
  - rbac.authorization.k8s.io
  resources:
  - rolebindings
  - clusterrolebindings
  - roles
  verbs:
  - create
  - get
  - list
  - patch
  - update
  - watch
  - delete
- apiGroups:
  - policy
  resources:
  - podsecuritypolicies
  verbs:
  - create
  - get
  - list
  - patch
  - update
  - use
  - delete
- apiGroups:
  - ""
  resources:
  - namespaces
  - configmaps
  - resourcequotas
  - limitranges
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - certificates.k8s.io
  resources:
  - certificatesigningrequests
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - replace
  - update
- apiGroups:
  - certificates.k8s.io
  resources:
  - certificatesigningrequests/status
  verbs:
  - get
- apiGroups:
  - certificates.k8s.io
  resources:
  - certificatesigningrequests/approval
  verbs:
  - update
- apiGroups:
  - certificates.k8s.io
  resources:
  - signers
  resourceNames:
  - kubernetes.io/kube-apiserver-client
  verbs:
  - approve
- apiGroups:
  - kyverno.io
  resources:
  - policies
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - replace
  - update
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: user-maintainer-binding
subjects:
- kind: ServiceAccount
  namespace: {{ .Release.Namespace }}
  name: user-maintainer
roleRef:
  kind: ClusterRole
  name: user-maintainer
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: user-maintainer-tools-user
subjects:
- kind: ServiceAccount
  namespace: {{ .Release.Namespace }}
  name: user-maintainer
roleRef:
  kind: ClusterRole
  name: tools-user
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: user-maintainer-k8s-admin
subjects:
- kind: ServiceAccount
  namespace: {{ .Release.Namespace }}
  name: user-maintainer
roleRef:
  kind: ClusterRole
  name: k8s-admin
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: user-maintainer-view
subjects:
- kind: ServiceAccount
  namespace: {{ .Release.Namespace }}
  name: user-maintainer
roleRef:
  kind: ClusterRole
  name: view
  apiGroup: rbac.authorization.k8s.io
