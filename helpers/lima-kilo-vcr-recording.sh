#!/bin/bash

# NOTE:
# this script should help in recording VCR test data on lima-kilo

set -o errexit
set -o pipefail
set -o nounset
set -v

runtime_hostname="$(hostname -s)"
unexpected_hostname="lima-kilo"
if [ "${runtime_hostname}" == "${unexpected_hostname}" ] ; then
    echo "ERROR: are you inside the lima-kilo VM? This script is expected to run on your laptop directly." >&2
    exit 1
fi

ROOT_DIR="$(git rev-parse --show-toplevel)"
LIMA_KILO_HOME="$(limactl --log-level error shell lima-kilo -- printenv HOME)"
LIMA_KILO_MAINTAIN_KUBEUSERS_DIR="${LIMA_KILO_HOME}/maintain-kubeusers"
CASSETTES_DIR="tests/cassettes"
CONTAINER_CASSETTES_DIR="/app/${CASSETTES_DIR}"
LAPTOP_REPO_CASSETTES_DIR="${ROOT_DIR}/${CASSETTES_DIR}"

# helper function to execute stuff inside lima-kilo
lexec() {
    eval limactl --log-level error shell --workdir "${LIMA_KILO_MAINTAIN_KUBEUSERS_DIR}" lima-kilo -- "$1"
}

# helper function to execute kubectl inside lima-kilo for the maintain-kubeusers namespace
k() {
    lexec "kubectl --namespace maintain-kubeusers $1"
}

# helper function to execute stuff inside the maintain-kubeusers container
kexec() {
    lexec "kubectl --namespace maintain-kubeusers exec -it deploy/maintain-kubeusers -- sh -c -- \"$1\""
}

# make sure we are in the root of the repo
cd "$ROOT_DIR"

# cleanup previous run, and copy our working directory inside lima-kilo
limactl --log-level error shell --workdir "${LIMA_KILO_HOME}" lima-kilo -- rm -rf maintain-kubeusers
limactl --log-level error shell --workdir "${LIMA_KILO_HOME}" lima-kilo -- mkdir -p maintain-kubeusers
limactl --log-level error copy --tty=false --recursive "${ROOT_DIR}" lima-kilo:"${LIMA_KILO_HOME}" >/dev/null

# build the test variant of the container image, load into lima-kilo's kind and deploy it
lexec "docker build -f Dockerfile.test -t mk-test:testcase ."
lexec "kind load docker-image mk-test:testcase -n toolforge"
# remove registry admission if there
lexec "helm -n registry-admission uninstall registry-admission" || :
# setsid makes the command non interactive, otherwise there is a prompt
lexec "setsid ./deploy.sh vcr-recording"

# in case the deploy script did not detect any change (run twice for example)
# force a restart of the deployment so it picks up the newer container image
k "rollout restart deploy/maintain-kubeusers"

# also, wait for it to complete, otherwise the next kexec commands will be
# executed inside the terminating container. This behavior should be fixed in k8s 1.28
# see https://github.com/kubernetes/kubectl/issues/1370
k "rollout status deploy/maintain-kubeusers"
while true ; do
    k "get pods -o jsonpath={.items[*].metadata.deletionTimestamp}" | grep -q "[0-9]" || break
    echo "waiting for terminating pod to go away ..."
    sleep 1
done

# cleanup pre-existing VCR assettes recordings
kexec "rm -rf ${CONTAINER_CASSETTES_DIR}/*"

# run the tests inside the container image
kexec "venv/bin/pytest --capture=no -v --in-k8s --vcr-record=all"

# remove local cassettes
git rm -f "${LAPTOP_REPO_CASSETTES_DIR}/test_*.yaml" || true

# get the new cassettes, from the container inside lima-kilo to the laptop's repo
for file in $(kexec "ls ${CONTAINER_CASSETTES_DIR}/") ; do
    kexec "cat ${CONTAINER_CASSETTES_DIR}/${file}" > "${LAPTOP_REPO_CASSETTES_DIR}/${file}"
done

# put registry admission back
lexec "setsid toolforge_deploy_mr.py registry-admission restore" || :
