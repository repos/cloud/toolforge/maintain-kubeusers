from dataclasses import dataclass


@dataclass(frozen=True)
class Account:
    name: str
    uid: int
    data_dir: str
    is_admin: bool
    namespace: str
    is_disabled: bool
    configmap_name: str

    def __str__(self) -> str:
        return (
            f"name: '{self.name}' uid: '{self.uid}' data_dir: '{self.data_dir}' is_admin: '{self.is_admin}' "
            f"namespace: '{self.namespace}' is_disabled: '{self.is_disabled}' configmap_name: '{self.configmap_name}'"
        )
