import argparse
import logging
import time
from pathlib import Path
from typing import Any

import ldap3
import yaml
from kubernetes import config as k_config
from prometheus_client import (
    Gauge,
    Summary,
    disable_created_metrics,
    start_http_server,
)

from .k8s_api import K8sAPI
from .quota import QuotaConfig, parse_quota_config_file
from .utils import get_admins_from_ldap, get_tools_from_ldap

"""
Automate the process of generating user credentials for Toolforge
Kubernetes


 - Get a list of all the users from LDAP
 - Get a list of namespaces/configmaps in k8s for each toolforge user
 - Do a diff, find new users and users with deleted configmaps
 - For each new user or removed configmap:
    - Create new namespace (only for a new user)
    - generate a CSR (including the right group for RBAC)
    - Validate and approve the CSR
    - Drop the .kube/config file in the tool directory
    - Annotate the namespace with configmap

"""


run_time = Summary(
    "maintain_kubeusers_run_seconds", "Time spent on maintain-kubeusers runs"
)
run_finished = Gauge(
    "maintain_kubeusers_run_finished",
    "Timestamp when the last maintain-kubeusers run finished",
)


def handle_k8s_liveness_probe() -> None:
    # Touch a temp file for a Kubernetes liveness check to prevent hangs
    Path("/tmp/run.check").touch()


@run_time.time()
def do_run(
    k8s_api: K8sAPI,
    ldap_config: dict[str, Any],
    ldap_servers: ldap3.ServerPool,
    project: str,
    quota: QuotaConfig,
) -> None:
    api_server, ca_data = k8s_api.get_cluster_info()

    with ldap3.Connection(
        ldap_servers,
        read_only=True,
        user=ldap_config["user"],
        auto_bind="DEFAULT",
        password=ldap_config["password"],
        raise_exceptions=True,
        receive_timeout=60,
    ) as conn:
        tools = get_tools_from_ldap(
            conn=conn,
            projectname=project,
            quota=quota,
            api_server=api_server,
            ca_data=ca_data,
        )
        admins = get_admins_from_ldap(
            conn=conn,
            projectname=project,
            api_server=api_server,
            ca_data=ca_data,
        )

    for tool in tools.values():
        handle_k8s_liveness_probe()
        tool.reconcile()

    for admin in admins.values():
        handle_k8s_liveness_probe()
        admin.reconcile()

    logging.info(
        f"finished run, reconciled {len(admins)} admins, {len(tools)} tool accounts"
    )


def main() -> None:
    disable_created_metrics()  # type: ignore

    argparser = argparse.ArgumentParser()
    group1 = argparser.add_mutually_exclusive_group()
    argparser.add_argument(
        "--ldapconfig",
        help="Path to YAML LDAP config file",
        default=Path("/etc/ldap.yaml"),
        type=Path,
    )
    argparser.add_argument(
        "--quota-config",
        help="Path to quota YAML quota config file",
        default=Path("/etc/maintain-kubeusers/quotas.yaml"),
        type=Path,
    )
    argparser.add_argument(
        "--debug", help="Turn on debug logging", action="store_true"
    )
    argparser.add_argument(
        "--project",
        help="Project name to fetch LDAP users from",
        default="tools",
    )
    group1.add_argument(
        "--interval",
        help="Seconds between between runs",
        default=1,
        type=int,
    )
    group1.add_argument("--once", help="Run once and exit", action="store_true")
    argparser.add_argument(
        "--local",
        help="Specifies this is not running in Kubernetes (for debugging)",
        action="store_true",
    )

    args = argparser.parse_args()

    loglvl = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format="%(message)s", level=loglvl)

    with args.ldapconfig.open("r", encoding="utf-8") as f:
        ldap_config = yaml.safe_load(f)

    ldap_servers = ldap3.ServerPool(
        [
            ldap3.Server(s, connect_timeout=1, get_info="ALL")
            for s in ldap_config["servers"]
        ],
        ldap3.ROUND_ROBIN,
        active=True,
        exhaust=True,
    )

    if args.local:
        k_config.load_kube_config()
    else:
        k_config.load_incluster_config()

    k8s_api = K8sAPI()

    start_http_server(9000)

    while True:
        logging.info("starting a run")
        handle_k8s_liveness_probe()

        quota_config = parse_quota_config_file(args.quota_config)

        do_run(
            k8s_api=k8s_api,
            ldap_servers=ldap_servers,
            ldap_config=ldap_config,
            project=args.project,
            quota=quota_config,
        )

        run_finished.set_to_current_time()

        if args.once:
            break

        time.sleep(args.interval)


if __name__ == "__main__":
    main()
