import logging

from kubernetes.client.rest import ApiException


class ResourceError(RuntimeError):
    """Exception that happens when a resource cannot be reconciled."""


def already_exists_or_raise(
    api_ex: ApiException, kind: str, name: str, ns: str | None = None
) -> None:
    ns_str = f" in namespace '{ns}'" if ns else ""
    if api_ex.status == 409 and "AlreadyExists" in api_ex.body:
        logging.info(f"{kind} '{name}' already exists{ns_str}")
    else:
        raise_k8s_resource_error(
            api_ex=api_ex, kind=kind, name=name, ns=ns, op="create"
        )


def already_deleted_or_raise(
    api_ex: ApiException, kind: str, name: str, ns: str | None = None
) -> None:
    ns_str = f" in namespace '{ns}'" if ns else ""
    if api_ex.status == 404:
        logging.info(f"{kind} '{name}' already deleted{ns_str}")
    else:
        raise_k8s_resource_error(
            api_ex=api_ex, kind=kind, name=name, ns=ns, op="delete"
        )


def raise_k8s_resource_error(
    api_ex: ApiException, kind: str, name: str, op: str, ns: str | None = None
) -> None:
    ns_str = f" in namespace '{ns}'" if ns else ""
    msg = f"resource error when interacting with the k8s API: could not {op} {kind} '{name}'{ns_str}: {api_ex}"
    raise_resource_error(ex=api_ex, msg=msg)


def raise_resource_error(
    msg: str,
    ex: Exception | None = None,
) -> None:
    logging.error(msg)
    if ex:
        raise ResourceError(msg) from ex
    else:
        raise ResourceError(msg)
