from typing import Tuple

import yaml
from kubernetes import client


class K8sAPI:
    def __init__(self) -> None:
        self.core = client.CoreV1Api()

    def get_cluster_info(self) -> Tuple[str, str]:
        c_info = self.core.read_namespaced_config_map(
            "cluster-info", "kube-public"
        )
        cl_kubeconfig = yaml.safe_load(c_info.data["kubeconfig"])
        ca_data = cl_kubeconfig["clusters"][0]["cluster"][
            "certificate-authority-data"
        ]
        api_server = cl_kubeconfig["clusters"][0]["cluster"]["server"]
        return api_server, ca_data
