from dataclasses import asdict, dataclass
from math import ceil
from pathlib import Path
from typing import Any

import yaml
from kubernetes.client import (
    V1LimitRangeItem,
    V1LimitRangeSpec,
    V1ResourceQuotaSpec,
)


@dataclass
class QuotaEntry:
    version: str
    """A string identifying the version of this quota configuration."""

    cpu: int
    """Quota for number of whole CPUs allocated."""

    memory: int
    """Quota for number of gibibytes of RAM."""

    pod_cpu: int
    """Limit for CPUs allocated to a single pod."""

    pod_memory: int
    """Limit for gibibytes of RAM allocated to a single pod."""

    pods: int
    """Quota for number of number of pods running at once."""

    jobs: int
    """Quota for jobs running or queued at once."""

    cronjobs: int
    """Quota for scheduled cron jobs."""

    deployments: int
    """Quota for deployments."""

    configmaps: int
    """Quota for config maps"""

    secrets: int
    """Quota for secrets."""

    services: int
    """Quota for services."""

    def to_quota_spec(self) -> V1ResourceQuotaSpec:
        return V1ResourceQuotaSpec(
            hard={
                "requests.cpu": f"{ceil(self.cpu / 2)}",
                "requests.memory": f"{ceil(self.memory / 2)}Gi",
                "limits.cpu": f"{self.cpu}",
                "limits.memory": f"{self.memory}Gi",
                "pods": f"{self.pods}",
                "services": f"{self.services}",
                "services.nodeports": "0",
                "secrets": f"{self.secrets}",
                "configmaps": f"{self.configmaps}",
                "persistentvolumeclaims": "0",
                "count/cronjobs.batch": f"{self.cronjobs}",
                "count/jobs.batch": f"{self.jobs}",
                "count/deployments.apps": f"{self.deployments}",
            }
        )

    def to_limit_range_spec(self) -> V1LimitRangeSpec:
        return V1LimitRangeSpec(
            limits=[
                V1LimitRangeItem(
                    default={"cpu": "500m", "memory": "512Mi"},
                    default_request={"cpu": "250m", "memory": "256Mi"},
                    type="Container",
                    max={
                        "cpu": f"{self.pod_cpu}",
                        "memory": f"{self.pod_memory}Gi",
                    },
                    min={"cpu": "50m", "memory": "100Mi"},
                )
            ]
        )

    def override(self, **kwargs: Any) -> "QuotaEntry":
        if "version" not in kwargs:
            raise ValueError("Quota overrides must override version")

        version = kwargs.pop("version")

        args = asdict(self)
        args.update(kwargs)
        args["version"] = f"{self.version}-{version}"

        return QuotaEntry(**args)


@dataclass(frozen=True)
class QuotaConfig:
    """Class holding the default quota configuration and any overrides."""

    default: QuotaEntry
    """The default quota."""

    overrides: dict[str, QuotaEntry]
    """Quota overrides keyed by user."""

    def get_for_user(self, user: str) -> QuotaEntry:
        return self.overrides.get(user, self.default)


def parse_quota_config_file(path: Path) -> QuotaConfig:
    with path.open("r", encoding="utf-8") as f:
        data = yaml.safe_load(f)

    default = QuotaEntry(**data["default"])
    overrides = {
        user: default.override(**override)
        for user, override in data.get("overrides", {}).items()
    }

    return QuotaConfig(
        default=default,
        overrides=overrides,
    )
