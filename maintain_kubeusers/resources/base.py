from abc import ABC, abstractmethod
from typing import Any


class ResourceError(RuntimeError):
    """Exception that happens when a resource cannot be reconciled."""


class BaseResource(ABC):
    """Base resource class. This is the interface that all resources must implement."""

    CONFIGMAP_KEY: str | None = None

    @abstractmethod
    def needs_create(self, resource_data: Any) -> bool:
        raise NotImplementedError

    @abstractmethod
    def do_create(self, resource_data: Any) -> Any:
        raise NotImplementedError

    @abstractmethod
    def needs_update(self, resource_data: Any) -> bool:
        raise NotImplementedError

    @abstractmethod
    def do_update(self, resource_data: Any) -> Any:
        raise NotImplementedError

    @abstractmethod
    def needs_delete(self, resource_data: Any) -> bool:
        raise NotImplementedError

    @abstractmethod
    def do_delete(self, resource_data: Any) -> None:
        raise NotImplementedError
