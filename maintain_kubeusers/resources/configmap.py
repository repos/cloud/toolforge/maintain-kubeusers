from typing import Any

from kubernetes import client
from kubernetes.client.rest import ApiException

from ..account import Account
from ..errors import (
    already_deleted_or_raise,
    already_exists_or_raise,
    raise_resource_error,
)
from .base import BaseResource


class Configmap(BaseResource):
    CONFIGMAP_KEY = "configmap"

    def __init__(self, account: Account):
        self.account = account
        self.core = client.CoreV1Api()

        # NOTE: versioning is not really implemented
        # but anyway, this helps detect if the configmap has been installed already
        self.installed_message = "installed"

    def needs_create(self, resource_data: Any) -> bool:
        if resource_data:
            return False

        return True

    def do_create(self, resource_data: Any) -> Any:
        core = client.CoreV1Api()
        config_map = client.V1ConfigMap(
            api_version="v1",
            kind="ConfigMap",
            metadata=client.V1ObjectMeta(name=self.account.configmap_name),
            data={},
        )
        try:
            core.create_namespaced_config_map(
                self.account.namespace, body=config_map
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="configmap",
                name=self.account.configmap_name,
                ns=self.account.namespace,
            )

        return self.installed_message

    def needs_update(self, resource_data: Any) -> bool:
        return False

    def do_update(self, resource_data: Any) -> Any:
        raise NotImplementedError

    def needs_delete(self, resource_data: Any) -> bool:
        # if a disabled admin, we delete here
        if self.account.is_disabled and self.account.is_admin:
            return True

        # if not admin, configmap it is deleted as part of namespace deletion
        return False

    def do_delete(self, resource_data: Any) -> None:
        if not self.account.is_admin:
            raise_resource_error(
                msg="trying to delete configmap of a non admin account"
            )

        try:
            self.core.delete_namespaced_config_map(
                name=self.account.configmap_name,
                namespace=self.account.namespace,
            )
        except ApiException as api_ex:
            already_deleted_or_raise(
                api_ex=api_ex,
                kind="configmap",
                name=self.account.configmap_name,
                ns=self.account.namespace,
            )
