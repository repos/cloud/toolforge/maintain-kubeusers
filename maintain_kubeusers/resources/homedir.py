import logging
import os
import pathlib
import shutil
import stat
from pathlib import Path
from typing import Any

from ..account import Account
from ..errors import raise_resource_error
from .base import BaseResource


class HomeDir(BaseResource):
    CONFIGMAP_KEY: str = "homedir"

    def __init__(self, account: Account) -> None:
        self.account = account
        self.home = account.data_dir
        self.disabled_flag = os.path.join(self.home, "k8s.disabled")
        self.installed_message = "installed"

    def needs_create(self, resource_data: Any) -> bool:
        if resource_data:
            return False

        return True

    def _copy_skel(self, mode: int) -> None:
        skel = Path("/etc/skel")
        if not skel.exists():
            return

        for dir_path, _, child_files in os.walk(skel):
            rel_dir = dir_path[len(str(skel)) + 1 :]
            if rel_dir != "":
                dest_dir = os.path.join(
                    self.home, dir_path[len(str(skel)) + 1 :]
                )
                os.makedirs(dest_dir, mode=mode, exist_ok=True)
                os.chown(dest_dir, self.account.uid, self.account.uid)
            else:
                dest_dir = self.home

            for child_file in child_files:
                dest_path = os.path.join(dest_dir, child_file)
                orig_path = os.path.join(dir_path, child_file)
                shutil.copy(orig_path, dest_path)
                os.chown(dest_path, self.account.uid, self.account.uid)

    def do_create(self, resource_data: Any) -> Any:
        # verify if this is a net new homedir, see NOTE below
        new_homedir = not os.path.exists(self.home)

        mode = 0o700 if self.account.is_admin else 0o775
        try:
            os.makedirs(self.home, mode=mode, exist_ok=True)
            os.chmod(self.home, mode | stat.S_ISGID)
            os.chown(self.home, self.account.uid, self.account.uid)

            logs_dir = os.path.join(self.home, "logs")
            os.makedirs(logs_dir, mode=mode, exist_ok=True)
            os.chmod(logs_dir, mode | stat.S_ISGID)
            os.chown(self.home, self.account.uid, self.account.uid)
        except Exception as ex:
            raise_resource_error(
                msg=f"problem creating homedir for account {self.account}: {ex}",
                ex=ex,
            )

        # use was previously disabled, now back to active: cleanup flag if it exists
        if not new_homedir:
            try:
                os.remove(self.disabled_flag)
            except FileNotFoundError:
                pass

        # NOTE:
        # the copy skel routine could potentially overwrite files in the homedir
        # therefore only copy it if this is a net new homedir.
        # additionally, don't copy the skel for admins. Admins are humans -- normal user accounts
        # we can let PAM create the homedir for them on first bastion login
        if new_homedir and not self.account.is_admin:
            try:
                self._copy_skel(mode=mode)
            except Exception as ex:
                raise_resource_error(
                    msg=f"problem copying homedir skel for account {self.account}: {ex}",
                    ex=ex,
                )

        return self.installed_message

    def needs_update(self, resource_data: Any) -> bool:
        return False

    def do_update(self, resource_data: Any) -> Any:
        raise NotImplementedError

    def needs_delete(self, resource_data: Any) -> bool:
        if (
            self.account.is_disabled
            and resource_data
            and os.path.exists(self.home)
        ):
            # NOTE: the user was marked as is_disabled()
            # do we need to perform any deletion action regarding the homedir? yes, but not deleting the homedir itself
            # but creating a filesystem flag. See do_delete()
            return not os.path.exists(self.disabled_flag)

        return False

    def do_delete(self, resource_data: Any) -> None:
        # NOTE: this is our way to delete an user from the homedir PoV: just create this disabled flag file
        # but only do it if a home directory for the user exists. It may have been deleted by other means?
        try:
            pathlib.Path(self.disabled_flag).touch()
        except FileNotFoundError as e:
            logging.warning(
                f"homedir: do_delete(): file not found error: {self.disabled_flag}: {e}"
            )
