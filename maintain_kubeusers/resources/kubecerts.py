import base64
import logging
import os
import random
import time
from datetime import UTC, datetime, timedelta, timezone
from typing import Any

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID
from kubernetes import client
from kubernetes.client.rest import ApiException

from ..account import Account
from ..errors import (
    already_deleted_or_raise,
    already_exists_or_raise,
    raise_k8s_resource_error,
    raise_resource_error,
)
from .base import BaseResource, ResourceError

# lifetime, in days for certificate validity
KUBECERT_LIFETIME_DAYS = 10
# lifetime, same, but in seconds
KUBECERT_LIFETIME_SECONDS = KUBECERT_LIFETIME_DAYS * 24 * 60 * 60
# time ahead in days to renew certificates.
KUBECERT_RENEW_DAYS_AHEAD = 2
# chances to renew a cert randomly
# this is about the amount of accounts we have (3500), so hopefully this
# triggers random renewal for at least one account on each run
KUBECERT_RENEW_RANDOM_CHANCE = 1 / 3500


class KubeCerts(BaseResource):
    CONFIGMAP_KEY: str = "kubecerts"

    def __init__(self, account: Account):
        self.account = account
        self.cdir_name = ".admkube" if account.is_admin else ".toolskube"
        self.location = os.path.join(self.account.data_dir, self.cdir_name)
        self.cert_file = os.path.join(self.location, "client.crt")
        self.key_file = os.path.join(self.location, "client.key")

    def _get_resource_data_expiration_date(self, resource_data: Any) -> Any:
        if not resource_data:
            return None

        status = resource_data.get("status")
        if not status or not isinstance(status, dict):
            return None

        return status.get("expires")

    def needs_create(self, resource_data: Any) -> bool:
        if self._get_resource_data_expiration_date(resource_data):
            return False

        return True

    def _generate_pk(self) -> rsa.RSAPrivateKey:
        return rsa.generate_private_key(
            public_exponent=65537, key_size=4096, backend=default_backend()
        )

    def _create_new_csr(
        self, certs: client.CertificatesV1Api, pk: rsa.RSAPrivateKey
    ) -> None:
        # TODO: see T365681 about validity
        org_name = "admins" if self.account.is_admin else "toolforge"
        csr = (
            x509.CertificateSigningRequestBuilder()
            .subject_name(
                x509.Name(
                    [
                        x509.NameAttribute(NameOID.ORGANIZATION_NAME, org_name),
                        x509.NameAttribute(
                            NameOID.COMMON_NAME, self.account.name
                        ),
                    ]
                )
            )
            .sign(pk, hashes.SHA256(), default_backend())
        )
        b64_csr = base64.b64encode(csr.public_bytes(serialization.Encoding.PEM))
        csr_spec = client.V1CertificateSigningRequestSpec(
            request=b64_csr.decode("utf-8"),
            groups=["system:authenticated", org_name],
            usages=["digital signature", "key encipherment", "client auth"],
            signer_name="kubernetes.io/kube-apiserver-client",
            expiration_seconds=KUBECERT_LIFETIME_SECONDS,
        )
        csr_body = client.V1CertificateSigningRequest(
            api_version="certificates.k8s.io/v1",
            kind="CertificateSigningRequest",
            metadata=client.V1ObjectMeta(name=self.account.name),
            spec=csr_spec,
        )

        # NOTE: don't catch this exception, as is used by _create_or_replace_csr()
        certs.create_certificate_signing_request(body=csr_body)

    def _create_or_replace_csr(
        self, certs: client.CertificatesV1Api, pk: rsa.RSAPrivateKey
    ) -> None:
        # The CSR must include the groups (which are org fields)
        # and CN of the user
        try:
            self._create_new_csr(certs=certs, pk=pk)
            return
        except ApiException as api_ex:
            # If maintain_kubeusers dies, a CSR may need cleaning up T271847
            already_exists_or_raise(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
            )

        # Clean up and try again
        try:
            certs.delete_certificate_signing_request(
                self.account.name, body=client.V1DeleteOptions()
            )
        except ApiException as api_ex:
            already_deleted_or_raise(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
            )

        try:
            self._create_new_csr(certs=certs, pk=pk)
            return
        except ApiException as api_ex:
            # NOTE: this is unexpected, not sure what should do here.
            # at this point, admin operations may be required if the CSR cannot be approved?
            already_exists_or_raise(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
            )

    def _approve_cert(self, certs: client.CertificatesV1Api) -> bytes:
        """Approve the CSR"""
        try:
            body = certs.read_certificate_signing_request_status(
                self.account.name
            )
        except ApiException as api_ex:
            raise_k8s_resource_error(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
                op="read_status",
            )

        # create an approval condition
        approval_condition = client.V1CertificateSigningRequestCondition(
            last_update_time=datetime.now(timezone.utc).astimezone(),
            message="This certificate was approved by maintain_kubeusers",
            reason="Authorized User",
            type="Approved",
            status="True",
        )
        # patch the existing `body` with the new conditions
        # you might want to append the new conditions to the existing ones
        body.status.conditions = [approval_condition]
        # Patch the Kubernetes CSR object in the certs API
        # The method called to the API is very confusingly named
        try:
            certs.replace_certificate_signing_request_approval(
                self.account.name, body
            )
        except ApiException as api_ex:
            raise_k8s_resource_error(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
                op="replace",
            )

        # There is a small delay in filling the certificate field, it seems.
        time.sleep(1)

        try:
            api_response = certs.read_certificate_signing_request(
                self.account.name
            )
        except ApiException as api_ex:
            raise_k8s_resource_error(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
                op="read",
            )

        if not api_response.status.certificate:
            raise ResourceError(
                f"Certificate creation stalled or failed for {self.account.name}"
            )

        # Get the actual cert
        cert = base64.b64decode(api_response.status.certificate)

        # Clean up the API
        try:
            certs.delete_certificate_signing_request(
                self.account.name, body=client.V1DeleteOptions()
            )
        except ApiException as api_ex:
            already_deleted_or_raise(
                api_ex=api_ex,
                kind="CertificateSigningRequest",
                name=self.account.name,
            )

        return cert

    def _write_certs(self, cert: bytes, pk: rsa.RSAPrivateKey) -> None:
        """Write certs to the filesystem"""
        # exist_ok=True is fine here, and not a security issue (Famous
        # last words?).
        mode = 0o700 if self.account.is_admin else 0o775
        os.makedirs(self.location, mode=mode, exist_ok=True)
        os.chown(self.location, self.account.uid, self.account.uid)

        try:
            # The x.509 cert is already ready to write
            with open(self.cert_file, "wb") as cert_file:
                cert_file.write(cert)
            os.chown(self.cert_file, self.account.uid, self.account.uid)
            os.chmod(self.cert_file, 0o400)

            # The private key is an object and needs serialization
            with open(self.key_file, "wb") as key_file:
                key_file.write(
                    pk.private_bytes(
                        encoding=serialization.Encoding.PEM,
                        format=serialization.PrivateFormat.TraditionalOpenSSL,
                        encryption_algorithm=serialization.NoEncryption(),
                    )
                )
            os.chown(self.key_file, self.account.uid, self.account.uid)
            os.chmod(self.key_file, 0o400)
        except Exception as e:
            raise_resource_error(
                ex=e,
                msg=f"Could not write cert files at {self.cert_file} or {self.key_file}: {e}",
            )

    def do_create(self, resource_data: Any) -> Any:
        certs = client.CertificatesV1Api()
        pk = self._generate_pk()
        self._create_or_replace_csr(certs=certs, pk=pk)
        cert = self._approve_cert(certs=certs)
        self._write_certs(cert=cert, pk=pk)

        cert_o = x509.load_pem_x509_certificate(cert, default_backend())
        expires = cert_o.not_valid_after.replace(tzinfo=None).isoformat(
            timespec="seconds"
        )
        status = {
            "status": {
                "expires": expires,
            }
        }

        return status

    def _is_expired(self, expiry_time: datetime) -> bool:
        now = datetime.now(UTC).replace(tzinfo=None)
        if expiry_time <= now:
            logging.info(
                f"{self.account.name}: renewing expired certificate since {expiry_time}"
            )
            return True

        return False

    def _is_expiry_time_within_renew_threshold(
        self, expiry_time: datetime
    ) -> bool:
        now = datetime.now(UTC).replace(tzinfo=None)
        max = expiry_time - timedelta(days=KUBECERT_RENEW_DAYS_AHEAD)
        if max <= now:
            logging.info(
                f"{self.account.name}: renewing certificate within {KUBECERT_RENEW_DAYS_AHEAD} days renew threshold"
            )
            return True

        return False

    # we are mostly using this function to gradually renew old certificates (with a lifetime of 1 year)
    # into certificates with KUBECERT_LIFETIME_DAYS. We can drop this function when we know for sure all certificates
    # have KUBECERT_LIFETIME_DAYS
    def _is_randomly_selected_for_renewal(self, expiry_time: datetime) -> bool:
        max_date = datetime.now(UTC).replace(tzinfo=None) + timedelta(
            days=KUBECERT_LIFETIME_DAYS
        )
        if expiry_time <= max_date:
            # don't randomly renew certs if they are already using the KUBECERT_LIFETIME_DAYS lifetime
            # i.e, only select randomly those certs that have the old 1 year lifetime
            return False

        if random.random() < KUBECERT_RENEW_RANDOM_CHANCE:
            logging.info(
                f"{self.account.name}: randomly renewing certificate before expiration"
            )
            return True

        return False

    def needs_update(self, resource_data: Any) -> bool:
        stored_date = self._get_resource_data_expiration_date(
            resource_data=resource_data
        )
        if not stored_date:
            return True

        expiry_time = datetime.strptime(stored_date, "%Y-%m-%dT%H:%M:%S")

        if self._is_expired(expiry_time=expiry_time):
            return True

        if self._is_expiry_time_within_renew_threshold(expiry_time=expiry_time):
            return True

        if self._is_randomly_selected_for_renewal(expiry_time=expiry_time):
            return True

        return False

    def do_update(self, resource_data: Any) -> Any:
        return self.do_create(resource_data=resource_data)

    def needs_delete(self, resource_data: Any) -> bool:
        # TODO: we don't usually delete old certs? or we do? I'm unsure
        return False

    def do_delete(self, resource_data: Any) -> None:
        raise NotImplementedError
