import fcntl
import logging
import os
from typing import Any

import yaml

from ..account import Account
from ..errors import raise_resource_error
from .base import BaseResource


class Kubeconfig(BaseResource):
    CONFIGMAP_KEY: str = "kubeconfig"

    def __init__(
        self, project: str, api_server: str, ca_data: str, account: Account
    ):
        self.account = account
        self.ctx = "toolforge" if project.startswith("tools") else project
        self.kubeuser = (
            f"tf-{account.name}"
            if project.startswith("tools")
            else f"{project}-{account.name}"
        )
        self.path = f"{account.data_dir}/.kube/config"
        self.cdir_name = ".admkube" if self.account.is_admin else ".toolskube"
        self.api_server = api_server
        self.ca_data = ca_data
        self.installed_message = "installed"

    def needs_create(self, resource_data: Any) -> bool:
        if resource_data:
            return False

        return True

    def do_create(self, resource_data: Any) -> Any:
        dirpath = os.path.join(self.account.data_dir, ".kube")
        # use relative paths to allow relocating the kubeconfig files and certs
        # as supported by the official libraries
        certpath = os.path.join("..", self.cdir_name)
        certfile = os.path.join(certpath, "client.crt")
        keyfile = os.path.join(certpath, "client.key")

        config = {
            "apiVersion": "v1",
            "kind": "Config",
            "clusters": [
                {
                    "cluster": {
                        "server": self.api_server,
                        "certificate-authority-data": self.ca_data,
                    },
                    "name": self.ctx,
                },
            ],
            "users": [
                {
                    "user": {
                        "client-certificate": certfile,
                        "client-key": keyfile,
                    },
                    "name": self.kubeuser,
                },
            ],
            "contexts": [
                {
                    "context": {
                        "cluster": self.ctx,
                        "user": self.kubeuser,
                        "namespace": self.account.namespace,
                    },
                    "name": self.ctx,
                }
            ],
            "current-context": self.ctx,
        }

        mode = 0o700 if self.account.is_admin else 0o775
        # exist_ok=True is fine here, and not a security issue (Famous
        # last words?).
        os.makedirs(dirpath, mode=mode, exist_ok=True)
        os.chown(dirpath, self.account.uid, self.account.uid)

        f = os.open(
            self.path, os.O_CREAT | os.O_WRONLY | os.O_NOFOLLOW | os.O_TRUNC
        )
        try:
            fcntl.flock(f, fcntl.LOCK_EX)
            os.write(
                f,
                yaml.safe_dump(
                    config, encoding="utf-8", default_flow_style=False
                ),
            )
            fcntl.flock(f, fcntl.LOCK_UN)
            # uid == gid
            os.fchown(f, self.account.uid, self.account.uid)
            os.fchmod(f, 0o600)
            logging.info(f"Wrote config in {self.path}")
        except os.error as e:
            raise_resource_error(ex=e, msg=f"Error creating {self.path}: {e}")
        finally:
            os.close(f)

        return self.installed_message

    def needs_update(self, resource_data: Any) -> bool:
        return False

    def do_update(self, resource_data: Any) -> Any:
        raise NotImplementedError

    def needs_delete(self, resource_data: Any) -> bool:
        return self.account.is_disabled and os.path.isfile(self.path)

    def do_delete(self, resource_data: Any) -> None:
        try:
            os.remove(self.path)
        except FileNotFoundError:
            # ok, already removed
            pass
