from pathlib import Path
from string import Template
from typing import Any

import yaml
from kubernetes import client
from kubernetes.client.rest import ApiException

from ..account import Account
from ..errors import (
    already_exists_or_raise,
    raise_k8s_resource_error,
    raise_resource_error,
)
from .base import BaseResource

CURDIR = Path(__file__).parent.absolute()
KYVERNO_POLICY_PATH = CURDIR / "kyverno_pod_policy.yaml.tpl"
KYVERNO_POLICY_TEMPLATE = Template(KYVERNO_POLICY_PATH.read_text())
KYVERNO_POLICY_VERSION_ANNOTATION = "toolforge.org/kyverno_pod_policy_version"


class KyvernoPodPolicy(BaseResource):

    CONFIGMAP_KEY: str = "kyverno_pod_policy"

    def __init__(self, account: Account) -> None:
        self.account = account
        self.policy = self._generate_policy()
        self.desired_version = str(
            self.policy["metadata"]["annotations"][
                KYVERNO_POLICY_VERSION_ANNOTATION
            ]
        )
        self.policy_name = self.policy["metadata"]["name"]
        self.policy_plural = "policies"
        api_version = self.policy["apiVersion"]
        self.policy_api_group = api_version.split("/")[0]
        self.policy_api_version = api_version.split("/")[1]

    def _generate_policy(self) -> Any:
        template_vars = {
            "NAMESPACE": self.account.namespace,
            "TOOL_NAME": self.account.name,
            "TOOL_DATA_DIR": self.account.data_dir,
            "TOOL_UID": self.account.uid,
        }

        try:
            yaml_string = KYVERNO_POLICY_TEMPLATE.substitute(template_vars)
            return yaml.safe_load(yaml_string)
        except Exception as ex:
            raise_resource_error(
                msg=f"error rendering kyverno pod policy template: {ex}", ex=ex
            )

    def needs_create(self, resource_data: Any) -> bool:
        if resource_data:
            # we got something created already, we don't need to create anything again
            return False

        if self.account.is_admin:
            # admins don't have a kyverno policy (they did not have a PSP either)
            return False

        return True

    def do_create(self, resource_data: Any) -> Any:
        api = client.CustomObjectsApi()
        try:
            api.create_namespaced_custom_object(
                group=self.policy_api_group,
                version=self.policy_api_version,
                plural=self.policy_plural,
                namespace=self.account.namespace,
                body=self.policy,
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="kyverno policy",
                name=self.policy_name,
                ns=self.account.namespace,
            )

        return self.desired_version

    def needs_update(self, resource_data: Any) -> bool:
        # admins don't have kyverno policy
        if self.account.is_admin:
            return False

        current_version = str(resource_data)
        return current_version != self.desired_version

    def do_update(self, resource_data: Any) -> Any:
        api = client.CustomObjectsApi()
        try:
            api.patch_namespaced_custom_object(
                name=self.policy_name,
                group=self.policy_api_group,
                version=self.policy_api_version,
                plural=self.policy_plural,
                namespace=self.account.namespace,
                body=self.policy,
            )
        except ApiException as api_ex:
            raise_k8s_resource_error(
                api_ex=api_ex,
                kind="kyverno policy",
                name=self.policy_name,
                op="patch",
                ns=self.account.namespace,
            )

        return self.desired_version

    def needs_delete(self, resource_data: Any) -> bool:
        # NOTE: not implemented.
        # normal accounts get this policy deleted with namespace deletion
        # admins don't have a policy
        return False

    def do_delete(self, resource_data: Any) -> None:
        raise NotImplementedError
