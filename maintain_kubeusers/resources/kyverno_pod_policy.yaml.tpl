---
# NOTE: this template, which contains a per-tool account policy, will be rendered and injected
# by maintain-kubeusers, then enforced by kyverno.
# See also:
# https://kyverno.io/docs/writing-policies/
# https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#podsecuritycontext-v1-core
# https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#securitycontext-v1-core
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  name: "toolforge-kyverno-pod-policy"
  namespace: "${NAMESPACE}"
  annotations:
    # NOTE: this annotation is used to track the policy version and decide if maintain-kubeusers needs to update it
    toolforge.org/kyverno_pod_policy_version: "7"
    pod-policies.kyverno.io/autogen-controllers: "none"
    policies.kyverno.io/title: "Toolforge ${TOOL_NAME} pod policy"
    policies.kyverno.io/description: "Toolforge tool account pod security mutation and validation"
    policies.kyverno.io/category: "Toolforge pod policy"
    policies.kyverno.io/subject: "Pod"

spec:
  validationFailureAction: "Enforce"
  rules:

  # this policy rule mutates the Pod resources to add required security controls
  - name: "toolforge-mutate-pod-policy"
    match:
      all:
      - resources:
          kinds:
          - Pod
          operations:
          # We are validating only during pod creation because k8s does not allow updating the securityContext of a running pod.
          # For more explanation see https://kubernetes.io/docs/concepts/workloads/pods/#pod-update-and-replacement
          - CREATE
    mutate:
      patchStrategicMerge:
        spec:
          hostNetwork: false
          hostIPC: false
          hostPID: false
          securityContext:
            runAsUser: ${TOOL_UID}
            runAsGroup: ${TOOL_UID}
            seccompProfile:
              type: "RuntimeDefault"
            fsGroup: ${TOOL_UID}
          containers:
            # this name here is required per kyverno docs, see
            # https://kyverno.io/docs/writing-policies/mutate/#strategic-merge-patch
            - (name): "*"
              securityContext:
                allowPrivilegeEscalation: false
                capabilities:
                  drop: [ALL]
                privileged: false
                runAsNonRoot: true

  # this policy rule validates that Pod resources have the right security controls
  - name: "toolforge-validate-pod-policy"
    match:
      all:
      - resources:
          kinds:
          - Pod
          operations:
          # We are validating only during pod creation because k8s does not allow updating the securityContext of a running pod.
          # For more explanation see https://kubernetes.io/docs/concepts/workloads/pods/#pod-update-and-replacement
          - CREATE
    validate:
      message: "pod security configuration must be correct"
      pattern:
        spec:
          =(workingDir): "${TOOL_DATA_DIR}"
          =(hostNetwork): false
          =(hostIPC): false
          =(hostPID): false
          securityContext:
            runAsUser: ${TOOL_UID}
            runAsGroup: ${TOOL_UID}
            =(seccompProfile):
              type: "RuntimeDefault"
            =(fsGroup): ${TOOL_UID}
            =(runAsNonRoot): true
            # prevent 0 (root) from being in here. This was injected by PSP, we may not reject it entirely yet
            =(supplementalGroups): 1-65535
          containers:
          - =(securityContext): &optional_securityContext
              =(allowPrivilegeEscalation): false
              =(capabilities):
                drop: [ALL]
              =(privileged): false
              =(runAsNonRoot): true
              =(seccompProfile):
                type: "RuntimeDefault"
              x(seLinuxOptions): null
              =(runAsUser): ${TOOL_UID}
              =(runAsGroup): ${TOOL_UID}
          =(initContainers):
          - =(securityContext): *optional_securityContext
          =(ephemerealContainers):
          - =(securityContext): *optional_securityContext
