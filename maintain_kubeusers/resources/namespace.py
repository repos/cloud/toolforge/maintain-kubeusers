from typing import Any

from kubernetes import client
from kubernetes.client.rest import ApiException

from ..account import Account
from ..errors import (
    already_deleted_or_raise,
    already_exists_or_raise,
    raise_resource_error,
)
from .base import BaseResource


class Namespace(BaseResource):
    CONFIGMAP_KEY = "namespace"

    def __init__(self, account: Account):
        self.account = account

        # NOTE: versioning is not really implemented
        # but anyway, this helps save k8s API hits
        self.installed_message = "created"

    def needs_create(self, resource_data: Any) -> bool:
        # the ns for admins is usually 'maintain-kubeusers', not created by this software
        if self.account.is_admin:
            return False

        # if there is data, it has been created
        # we don't check for any particular value though,
        # that would belong to the needs_update() routine
        if resource_data:
            return False

        return True

    def do_create(self, resource_data: Any) -> Any:
        core = client.CoreV1Api()
        try:
            core.create_namespace(
                body=client.V1Namespace(
                    api_version="v1",
                    kind="Namespace",
                    metadata=client.V1ObjectMeta(
                        name=self.account.namespace,
                        labels={
                            "name": self.account.namespace,
                            "tenancy": "tool",
                        },
                    ),
                )
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex, kind="namespace", name=self.account.namespace
            )

        return self.installed_message

    def needs_update(self, resource_data: Any) -> bool:
        # NOTE: feel free to change this if required
        return False

    def do_update(self, resource_data: Any) -> Any:
        # NOTE: feel free to change this if required
        raise NotImplementedError

    def needs_delete(self, resource_data: Any) -> bool:
        # warning, admins do their resource tracking on a shared configmap in the maintain-kubeusers namespace
        # therefore we never want to delete their namespace (as it is shared with other accounts.)
        # this return is preventing the very verbose error in _assert_not_namespace()
        if self.account.is_admin:
            return False

        return self.account.is_disabled and resource_data

    def _assert_not_namespace(self, ns: str) -> None:
        if self.account.namespace != ns:
            return

        msg = (
            f"BUG DETECTED: maintain-kubeusers was about to delete the wrong namespace '{ns}'. "
            "This is a coding error somewhere, likely related to resource tracking, "
            f"and the fact that admin accounts have their namespace set to '{ns}'. "
            "To solve this problem, you should read the code to find how this was even possible. "
            "Maybe start in this very file."
        )

        raise_resource_error(msg=msg)

    def _check_namespace(self) -> None:
        # verify we don't try to delete the namespace in which the daemon itself is likely running
        self._assert_not_namespace("maintain-kubeusers")

        ns_file = "/run/secrets/kubernetes.io/serviceaccount/namespace"
        try:
            with open(ns_file) as f:
                ns = f.read().strip()
                # verify we don't try to delete the namespace in which we are actually running
                self._assert_not_namespace(ns)
        except FileNotFoundError:
            # this can happen while developing this software, because maybe not running inside k8s
            pass

    def do_delete(self, resource_data: Any) -> None:
        self._check_namespace()
        core = client.CoreV1Api()
        try:
            core.delete_namespace(self.account.namespace)
        except ApiException as api_ex:
            already_deleted_or_raise(
                api_ex=api_ex, kind="namespace", name=self.account.namespace
            )
