from typing import Any

from kubernetes import client
from kubernetes.client.rest import ApiException

from ..account import Account
from ..errors import (
    already_exists_or_raise,
    raise_k8s_resource_error,
    raise_resource_error,
)
from ..quota import QuotaEntry
from .base import BaseResource

noquota = QuotaEntry("x", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)


class Quota(BaseResource):
    CONFIGMAP_KEY = "quota_version"

    def __init__(self, account: Account, quotaentry: QuotaEntry | None):
        self.account = account
        # placeholder, so mypy don't complains about it being None
        # we check via is_admin anyway
        self.quotaentry = quotaentry if quotaentry else noquota

        if self.quotaentry == noquota and not self.account.is_admin:
            msg = f"account {self.account} expected to have a quota but it is None"
            raise_resource_error(msg=msg)

    def needs_create(self, resource_data: Any) -> bool:
        # admins don't have quotas
        if self.account.is_admin:
            return False

        # if there is an entry in the resource_data already, we are good
        if resource_data:
            return False

        return True

    def do_create(self, resource_data: Any) -> Any:
        core = client.CoreV1Api()
        try:
            core.create_namespaced_resource_quota(
                namespace=self.account.namespace,
                body=client.V1ResourceQuota(
                    api_version="v1",
                    kind="ResourceQuota",
                    metadata=client.V1ObjectMeta(name=self.account.namespace),
                    spec=self.quotaentry.to_quota_spec(),
                ),
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="resourcequota",
                name=self.account.name,
                ns=self.account.namespace,
            )

        try:
            core.create_namespaced_limit_range(
                namespace=self.account.namespace,
                body=client.V1LimitRange(
                    api_version="v1",
                    kind="LimitRange",
                    metadata=client.V1ObjectMeta(name=self.account.namespace),
                    spec=self.quotaentry.to_limit_range_spec(),
                ),
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="limitrange",
                name=self.account.namespace,
                ns=self.account.namespace,
            )

        return self.quotaentry.version

    def needs_update(self, resource_data: Any) -> bool:
        # admins don't have quotas
        if self.account.is_admin:
            return False

        current_quota_version = str(resource_data)
        desired_quota_version = self.quotaentry.version

        return current_quota_version != desired_quota_version

    def do_update(self, resource_data: Any) -> Any:
        core = client.CoreV1Api()
        try:
            core.replace_namespaced_resource_quota(
                name=self.account.namespace,
                namespace=self.account.namespace,
                body=client.V1ResourceQuota(
                    api_version="v1",
                    kind="ResourceQuota",
                    metadata=client.V1ObjectMeta(name=self.account.namespace),
                    spec=self.quotaentry.to_quota_spec(),
                ),
            )
        except ApiException as api_ex:
            raise_k8s_resource_error(
                api_ex=api_ex,
                kind="resourcequota",
                name=self.account.namespace,
                ns=self.account.namespace,
                op="replace",
            )

        try:
            core.replace_namespaced_limit_range(
                name=self.account.namespace,
                namespace=self.account.namespace,
                body=client.V1LimitRange(
                    api_version="v1",
                    kind="LimitRange",
                    metadata=client.V1ObjectMeta(name=self.account.namespace),
                    spec=self.quotaentry.to_limit_range_spec(),
                ),
            )
        except ApiException as api_ex:
            raise_k8s_resource_error(
                api_ex=api_ex,
                kind="resourcequota",
                name=self.account.namespace,
                ns=self.account.namespace,
                op="replace",
            )

        return self.quotaentry.version

    def needs_delete(self, resource_data: Any) -> bool:
        # if account is an admin, it doesn't have quota, so no need to delete
        # if account is tool account, quotas are namespaced and go away with namespace deletion
        return False

    def do_delete(self, resource_data: Any) -> None:
        raise NotImplementedError
