from typing import Any

from kubernetes import client
from kubernetes.client import RbacAuthorizationV1Api
from kubernetes.client.rest import ApiException

from ..account import Account
from ..errors import (
    already_deleted_or_raise,
    already_exists_or_raise,
    raise_resource_error,
)
from .base import BaseResource


class RBAC(BaseResource):
    CONFIGMAP_KEY = "rbac"

    def __init__(self, account: Account):
        self.account = account
        self.tool_role_binding = f"{self.account.name}-tool-binding"
        self.admin_read_clusterrole_binding = (
            f"{self.account.name}-view-binding"
        )
        self.admin_sudo_clusterrole_binding = f"{self.account.name}-binding"

        # NOTE: versioning is not really implemented
        # but anyway, this helps detect if RBAC has been installed already, saving API hits, see below
        self.installed_message = "installed"

    def _get_admin_read_clusterrole_binding(
        self,
    ) -> client.V1ClusterRoleBinding:
        return client.V1ClusterRoleBinding(
            api_version="rbac.authorization.k8s.io/v1",
            kind="ClusterRoleBinding",
            metadata=client.V1ObjectMeta(
                name=self.admin_read_clusterrole_binding,
            ),
            role_ref=client.V1RoleRef(
                kind="ClusterRole",
                name="view",
                api_group="rbac.authorization.k8s.io",
            ),
            subjects=[
                client.V1Subject(
                    kind="User",
                    name=self.account.name,
                    api_group="rbac.authorization.k8s.io",
                )
            ],
        )

    def _get_admin_sudo_clusterrole_binding(
        self,
    ) -> client.V1ClusterRoleBinding:
        return client.V1ClusterRoleBinding(
            api_version="rbac.authorization.k8s.io/v1",
            kind="ClusterRoleBinding",
            metadata=client.V1ObjectMeta(
                name=self.admin_sudo_clusterrole_binding
            ),
            role_ref=client.V1RoleRef(
                kind="ClusterRole",
                name="k8s-admin",
                api_group="rbac.authorization.k8s.io",
            ),
            subjects=[
                client.V1Subject(
                    kind="User",
                    name=self.account.name,
                    api_group="rbac.authorization.k8s.io",
                )
            ],
        )

    def _get_tool_role_binding(self) -> client.V1RoleBinding:
        return client.V1RoleBinding(
            api_version="rbac.authorization.k8s.io/v1",
            kind="RoleBinding",
            metadata=client.V1ObjectMeta(
                name=self.tool_role_binding,
                namespace=self.account.namespace,
            ),
            role_ref=client.V1RoleRef(
                kind="ClusterRole",
                name="tools-user",
                api_group="rbac.authorization.k8s.io",
            ),
            subjects=[
                client.V1Subject(
                    kind="User",
                    name=self.account.name,
                    api_group="rbac.authorization.k8s.io",
                )
            ],
        )

    def needs_create(self, resource_data: Any) -> bool:
        if resource_data:
            return False

        return True

    def _do_create_admin(self, rbac: RbacAuthorizationV1Api) -> None:
        try:
            rbac.create_cluster_role_binding(
                body=self._get_admin_read_clusterrole_binding(),
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="clusterrolebinding",
                name=self.admin_read_clusterrole_binding,
            )

        try:
            rbac.create_cluster_role_binding(
                body=self._get_admin_sudo_clusterrole_binding(),
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="clusterrolebinding",
                name=self.admin_sudo_clusterrole_binding,
            )

    def _do_create_tool(self, rbac: RbacAuthorizationV1Api) -> None:
        try:
            rbac.create_namespaced_role_binding(
                namespace=self.account.namespace,
                body=self._get_tool_role_binding(),
            )
        except ApiException as api_ex:
            already_exists_or_raise(
                api_ex=api_ex,
                kind="rolebinding",
                name=self.tool_role_binding,
                ns=self.account.namespace,
            )

    def do_create(self, resource_data: Any) -> Any:
        rbac = client.RbacAuthorizationV1Api()
        if self.account.is_admin:
            self._do_create_admin(rbac=rbac)
        else:
            self._do_create_tool(rbac=rbac)

        return self.installed_message

    def needs_update(self, resource_data: Any) -> bool:
        # NOTE: not implemented, but you do if required.
        return False

    def do_update(self, resource_data: Any) -> Any:
        # NOTE: not implemented. But you do if required.
        raise NotImplementedError

    def needs_delete(self, resource_data: Any) -> bool:
        if not self.account.is_disabled:
            return False

        # admins needs the RBAC setup removed here. However, tools get it removed with the namespace
        if self.account.is_admin:
            return True

        return False

    def _delete_cluster_role_binding(
        self, rbac: client.RbacAuthorizationV1Api, name: str
    ) -> None:
        try:
            rbac.delete_cluster_role_binding(
                name=name,
                body=client.V1DeleteOptions(api_version="v1"),
            )
        except ApiException as api_ex:
            already_deleted_or_raise(
                api_ex=api_ex, kind="clusterrolebinding", name=name
            )

    def do_delete(self, resource_data: Any) -> None:
        if not self.account.is_admin:
            raise_resource_error(
                msg=(
                    "can't delete admin clusterrole bindings for a non-admin account, likely a bug. "
                    f"some debug info: account: {self.account}, resource_data: {resource_data}"
                )
            )

        rbac = client.RbacAuthorizationV1Api()
        self._delete_cluster_role_binding(
            rbac=rbac, name=self.admin_read_clusterrole_binding
        )
        self._delete_cluster_role_binding(
            rbac=rbac, name=self.admin_sudo_clusterrole_binding
        )
