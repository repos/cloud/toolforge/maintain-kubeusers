import logging
from typing import Any

import yaml
from kubernetes import client
from kubernetes.client.rest import ApiException
from prometheus_client import Counter

from ..account import Account
from ..errors import ResourceError
from ..quota import QuotaEntry
from .base import BaseResource
from .configmap import Configmap
from .homedir import HomeDir
from .kubecerts import KubeCerts
from .kubeconfig import Kubeconfig
from .kyverno_pod_policy import KyvernoPodPolicy
from .namespace import Namespace
from .quota import Quota
from .rbac import RBAC


def resource_name(resource: BaseResource | None) -> str:
    if not resource:
        return "unknown"

    return str(resource.__class__.__name__.lower())


resources_counter = Counter(
    name="maintain_kubeusers_resources",
    documentation="Data about resources managed by maintain_kubeusers",
    labelnames=[
        "account_type",
        "account_name",
        "operation",
    ],
)


def inc_counter(account: Account, operation: str) -> None:
    resources_counter.labels(
        account_type="admin" if account.is_admin else "tool",
        account_name=account.name,
        operation=operation,
    ).inc(1)


resources_error_counter = Counter(
    name="maintain_kubeusers_resources_errors",
    documentation="Data about resources errors managed by maintain_kubeusers",
    labelnames=[
        "account_type",
        "account_name",
        "operation",
        "resource",
    ],
)


def inc_error_counter(
    account: Account,
    operation: str,
    resource: BaseResource | None,
    error: ResourceError | None = None,
) -> None:
    if error:
        logging.error(
            f"account: {account} resource: '{resource_name(resource)}': error: '{error}'"
        )

    resources_error_counter.labels(
        account_type="admin" if account.is_admin else "tool",
        account_name=account.name,
        operation=operation,
        resource=resource_name(resource),
    ).inc(1)


class Resources:
    """A resource is something that needs to be created, associated with a given account.
    This class includes a reconcile() function that will iterate the list of such resources.

    Each resource is allowed to have some 'stateful' data, that will be stored in a maintain-kubeuser
    configmap. In general, such resource data will contain things like the version of the resource
    so we can track if we need to update the resource to a newer version.

    If a resource doesn't require to store anything in the configmap, they just set CONFIGMAP_KEY
    to None.

    If the account is a normal tool account, the configmap will live on its own namespace
    If the account is an admin account, the configmap will live in the maintain-kubeusers namespace
    """

    def __init__(self, account: Account, resources: list[BaseResource]):
        self.core = client.CoreV1Api()
        self.account = account
        self.resources = resources

    @classmethod
    def load(
        cls,
        account: Account,
        project: str,
        quota: QuotaEntry | None,
        api_server: str,
        ca_data: str,
    ) -> "Resources":
        resources: list[BaseResource] = []
        # NOTE: order mathers for new accounts: namespace and configmap should usually go first
        resources.append(Namespace(account=account))
        resources.append(Configmap(account=account))
        resources.append(HomeDir(account=account))
        resources.append(
            Kubeconfig(
                account=account,
                project=project,
                api_server=api_server,
                ca_data=ca_data,
            )
        )
        resources.append(KubeCerts(account=account))
        resources.append(RBAC(account=account))
        resources.append(Quota(account=account, quotaentry=quota))
        resources.append(KyvernoPodPolicy(account=account))

        return cls(account=account, resources=resources)

    def _log_operation(self, resource: BaseResource, operation: str) -> None:
        logging.info(
            f"account: '{self.account.name}' resource: '{resource_name(resource)}' needs {operation}"
        )

    def _reconcile_delete(
        self, resource: BaseResource, resource_data: Any
    ) -> None:
        operation = "delete"
        self._log_operation(resource=resource, operation=operation)
        try:
            resource.do_delete(resource_data)
            inc_counter(self.account, operation=operation)
        except ResourceError as error:
            inc_error_counter(
                self.account,
                operation=operation,
                resource=resource,
                error=error,
            )
            # don't modify the configmap, so most likely we try again the deletion
            return

        self._configmap_delete(resource=resource)

    def _reconcile_create(
        self, resource: BaseResource, resource_data: Any
    ) -> None:
        operation = "create"
        self._log_operation(resource=resource, operation=operation)
        try:
            resource_data = resource.do_create(resource_data)
            inc_counter(self.account, operation=operation)
        except ResourceError as error:
            inc_error_counter(
                self.account,
                operation=operation,
                resource=resource,
                error=error,
            )
            # don't modify the configmap, so most likely we try again the operation
            return

        self._configmap_update(resource=resource, cm_new_value=resource_data)

    def _reconcile_update(
        self, resource: BaseResource, resource_data: Any
    ) -> None:
        operation = "update"
        self._log_operation(resource=resource, operation=operation)
        try:
            resource_data = resource.do_update(resource_data)
            inc_counter(self.account, operation=operation)
        except ResourceError as error:
            inc_error_counter(
                self.account,
                operation=operation,
                resource=resource,
                error=error,
            )
            # don't modify the configmap, so most likely we try again the operation
            return

        self._configmap_update(resource=resource, cm_new_value=resource_data)

    def reconcile(self) -> None:
        logging.debug(f"reconcile() account: {self.account}")
        self._configmap_load()

        for resource in self.resources:
            resource_data = self._configmap_get_resource_data(resource)

            logging.debug(
                f"reconcile() account: '{self.account.name}' resource: '{resource_name(resource)}' key: '{resource.CONFIGMAP_KEY}' data: '{resource_data}'"
            )

            if resource.needs_delete(resource_data):
                self._reconcile_delete(
                    resource=resource,
                    resource_data=resource_data,
                )
                continue

            if self.account.is_disabled:
                logging.debug(
                    f"reconcile() account: '{self.account.name}' resource: '{resource_name(resource)}': "
                    "not evaluating create or update for disabled account"
                )
                continue

            if resource.needs_create(resource_data):
                self._reconcile_create(
                    resource=resource,
                    resource_data=resource_data,
                )
                continue

            if resource.needs_update(resource_data):
                self._reconcile_update(
                    resource=resource,
                    resource_data=resource_data,
                )
                continue

            logging.debug(
                f"reconcile() account '{self.account.name}': resource: '{resource_name(resource)}': no actions required"
            )

    def _configmap_load(self) -> None:

        try:
            cm = self.core.read_namespaced_config_map(
                name=self.account.configmap_name,
                namespace=self.account.namespace,
            )
        except ApiException as api_ex:
            cm = None
            # 404 may happen if this is a new account and the configmap has not been created yet
            if api_ex.status != 404:
                logging.error(
                    f"reconcile() _configmap_load() error reading configmap: {api_ex}"
                )
                inc_error_counter(
                    self.account,
                    operation="configmap_get_resource_data",
                    resource=None,
                )

        self.configmap_data = (
            cm.data if hasattr(cm, "data") and isinstance(cm.data, dict) else {}
        )

    def _configmap_get_resource_data(self, resource: BaseResource) -> Any:
        cm_key = resource.CONFIGMAP_KEY

        if not cm_key:
            return None

        data = self.configmap_data.get(cm_key)
        if not data:
            logging.debug(
                f"reconcile() _configmap_get_resource_data() no data for key '{cm_key}'"
            )
            return None

        try:
            return yaml.safe_load(data)
        except Exception as e:
            logging.error(
                f"reconcile() _configmap_get_resource_data() not valid YAML in key '{cm_key}': {data}: {e}"
            )
            inc_error_counter(
                self.account,
                operation="configmap_get_resource_data",
                resource=resource,
            )

    def _configmap_update(
        self, resource: BaseResource, cm_new_value: Any
    ) -> None:
        cm_key = resource.CONFIGMAP_KEY

        if not cm_key or not cm_new_value:
            return

        logging.debug(
            f"reconcile() _configmap_update() key '{cm_key}' data '{cm_new_value}'"
        )

        try:
            value = yaml.safe_dump(cm_new_value)
        except Exception as e:
            logging.error(
                f"reconcile() _configmap_update() not valid YAML in key '{cm_key}': {cm_new_value}. {e}"
            )
            inc_error_counter(
                self.account, operation="configmap_update", resource=resource
            )
            return

        try:
            self.core.patch_namespaced_config_map(
                name=self.account.configmap_name,
                namespace=self.account.namespace,
                body=client.V1ConfigMap(
                    api_version="v1",
                    kind="ConfigMap",
                    metadata=client.V1ObjectMeta(
                        name=self.account.configmap_name
                    ),
                    data={cm_key: value},
                ),
            )
        except ApiException as api_ex:
            if api_ex.status == 404 and cm_key == Namespace.CONFIGMAP_KEY:
                # this happens in the very first reconcile() loop for a given account if this account is being created.
                logging.debug(
                    f"did not update configmap {self.account.configmap_name} because it doesn't exist yet, "
                    f"the namespace {self.account.namespace} was just created"
                )
                return
            logging.error(
                f"failed to update configmap {self.account.configmap_name} "
                f"in namespace {self.account.namespace} for key '{cm_key}' with value: '{value}': {api_ex}"
            )
            inc_error_counter(
                account=self.account,
                operation="configmap_update_2",
                resource=resource,
            )
            return

        self.configmap_data[cm_key] = cm_new_value

    def _configmap_delete(self, resource: BaseResource) -> None:
        cm_key = resource.CONFIGMAP_KEY

        if not cm_key:
            return

        try:
            self.configmap_data.pop(cm_key)
        except KeyError:
            # that's ok, there was no data, so nothing to delete in k8s
            return

        patch_body = [{"op": "remove", "path": f"/data/{cm_key}"}]
        try:
            self.core.patch_namespaced_config_map(
                name=self.account.configmap_name,
                namespace=self.account.namespace,
                body=patch_body,
            )
        except ApiException as api_ex:
            if api_ex.status == 404:
                # this can actually happen if deleting the namespace first
                logging.info(
                    f"not deleting key {cm_key} from configmap {self.account.configmap_name} in ns {self.account.namespace} because it doesn't exist"
                )
            else:
                logging.error(
                    f"unexpected failure when trying to delete from configmap {self.account.configmap_name} in ns {self.account.namespace}: {api_ex}"
                )
                inc_error_counter(
                    account=self.account,
                    operation="configmap_delete",
                    resource=resource,
                )
