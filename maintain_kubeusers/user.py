from __future__ import annotations

from .account import Account
from .quota import QuotaEntry
from .resources.resources import Resources


class User:
    """Simple user object"""

    def __init__(
        self,
        name: str,
        uid: int,
        home: str,
        api_server: str,
        ca_data: str,
        pwdAccountLockedTime: str | None,
        pwdPolicySubentry: str | None,
        admin: bool = False,
        project: str = "tools",
        quota: QuotaEntry | None = None,
    ) -> None:
        is_disabled = (
            True if pwdPolicySubentry or pwdAccountLockedTime else False
        )
        configmap_name = f"maintain-kubeusers-{name}"
        # admins do resource tracking in the maintain-kubeusers namespace
        # tool accounts do resource tracking on a configmap on their own namespace
        ns = "maintain-kubeusers" if admin else f"tool-{name}"

        self.quota = quota
        self.account = Account(
            namespace=ns,
            configmap_name=configmap_name,
            uid=uid,
            name=name,
            data_dir=home,
            is_admin=admin,
            is_disabled=is_disabled,
        )

        # TODO: exists_in_ldap ?
        self.resources = Resources.load(
            account=self.account,
            project=project,
            quota=self.quota,
            api_server=api_server,
            ca_data=ca_data,
        )

    def reconcile(self) -> None:
        self.resources.reconcile()
