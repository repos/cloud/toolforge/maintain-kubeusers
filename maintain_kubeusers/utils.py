import logging
import re
from functools import cache
from typing import Dict

import ldap3

from maintain_kubeusers.quota import QuotaConfig
from maintain_kubeusers.user import User

UserDict = Dict[str, User]


dns_regex = re.compile(r"^[a-z0-9]([-a-z0-9]*[a-z0-9])?$")


# unbounded cache, we don't have that many account names
@cache
def check_account_name(account_name: str) -> bool:
    """tool names must conform to RFC 1123 as a DNS label
    For our purposes, they must also be no more than 54 characters in length.
    In some cases, dots are allowed, but it shouldn't be in the tool name.
    """
    return bool(dns_regex.match(account_name)) and len(account_name) < 54


def get_tools_from_ldap(
    conn: ldap3.Connection,
    projectname: str,
    api_server: str,
    ca_data: str,
    quota: QuotaConfig,
) -> UserDict:
    """
    Builds list of all tools from LDAP
    """
    tools: UserDict = {}
    entries = conn.extend.standard.paged_search(
        "ou=people,ou=servicegroups,dc=wikimedia,dc=org",
        "(&(objectClass=posixAccount)(cn={}.*))".format(projectname),
        search_scope=ldap3.SUBTREE,
        get_operational_attributes=True,
        attributes=["cn", "uidNumber", "homeDirectory"],
        time_limit=5,
        paged_size=500,
    )
    for entry in entries:
        attrs = entry["attributes"]
        user_name = attrs["cn"][0][len(projectname) + 1 :]
        if not check_account_name(user_name):
            logging.warning(f"invalid account name '{user_name}', skipping")
            continue

        tool = User(
            name=user_name,
            uid=attrs["uidNumber"],
            home=attrs["homeDirectory"],
            project=projectname,
            pwdAccountLockedTime=attrs.get("pwdAccountLockedTime", None),
            pwdPolicySubentry=attrs.get("pwdPolicySubentry", None),
            quota=quota.get_for_user(user_name),
            api_server=api_server,
            ca_data=ca_data,
        )
        maybe_existing = tools.get(tool.account.name)
        if maybe_existing:
            logging.error(
                f"duplicated account detected in LDAP, entry for '{tool.account.name}'. Check LDAP tree."
            )
            logging.error(
                f"duplicated account, version A: {maybe_existing.account}"
            )
            logging.error(f"duplicated account, version B: {tool.account}")
            logging.error("skipping version B and using version A")
            continue

        tools[tool.account.name] = tool

    return tools


def get_admins_from_ldap(
    conn: ldap3.Connection, projectname: str, api_server: str, ca_data: str
) -> UserDict:
    """
    Returns a list of project admins
    """
    admins = {}
    entries = conn.extend.standard.paged_search(
        "ou=servicegroups,dc=wikimedia,dc=org",
        "(&(objectClass=posixGroup)(cn={}.admin))".format(projectname),
        search_scope=ldap3.SUBTREE,
        get_operational_attributes=True,
        attributes=["member"],
        time_limit=5,
        paged_size=500,
    )
    for entry in entries:
        for member in entry["attributes"]["member"]:
            uid = member.split(",")[0]
            admin_gen = conn.extend.standard.paged_search(
                "ou=people,dc=wikimedia,dc=org",
                "(&(objectClass=posixAccount)({}))".format(uid),
                search_scope=ldap3.SUBTREE,
                attributes=["uid", "uidNumber", "homeDirectory"],
                time_limit=5,
                paged_size=500,
            )
            attrs = next(admin_gen)["attributes"]
            name = attrs["uid"][0]
            if not check_account_name(name):
                logging.warning(
                    f"invalid admin account name '{name}', skipping"
                )
                continue

            admin = User(
                name=name,
                uid=attrs["uidNumber"],
                home=attrs["homeDirectory"],
                pwdAccountLockedTime=attrs.get("pwdAccountLockedTime", None),
                pwdPolicySubentry=attrs.get("pwdPolicySubentry", None),
                admin=True,
                project=projectname,
                api_server=api_server,
                ca_data=ca_data,
            )
            admins[admin.account.name] = admin

    return admins
