import os
from pathlib import Path
from time import sleep
from typing import Any, Generator

import ldap3
import pytest
from kubernetes import client, config
from kubernetes.client.rest import ApiException
from pytest import FixtureRequest

from maintain_kubeusers.account import Account
from maintain_kubeusers.quota import QuotaConfig, QuotaEntry


@pytest.fixture(scope="session")
def vcr_cassette_dir(request: Any) -> str:
    # put all cassettes in tests/cassettes/{test}.yaml
    # see also: https://pytest-vcr.readthedocs.io/en/latest/#changing-the-cassettes-library-path
    return "tests/cassettes/"


@pytest.fixture(scope="session")
def vcr_config() -> dict[str, Any]:
    # make VCR see the same inside / outside kubernete.
    # also, replace the Authorization request header with "DUMMY" in cassettes
    # the default matcher is ['method', 'scheme', 'host', 'port', 'path', 'query']
    # so we are only dropping 'host' and 'port'
    # see also: https://vcrpy.readthedocs.io/en/latest/configuration.html#request-matching
    return {
        "filter_headers": [("authorization", "DUMMY")],
        "match_on": ["method", "scheme", "path", "query"],
    }


@pytest.fixture(scope="session", autouse=True)
def k8s_auth(try_local_kubeconfig: bool) -> None:
    # try first a local kubeconfig (useful for local dev)
    if try_local_kubeconfig:
        try:
            config.load_kube_config()
            return
        except Exception:
            pass

    # try next an incluster config (useful for in-cluster dev, like lima-kilo)
    try:
        config.load_incluster_config()
        return
    except Exception:
        pass

    # finally, a dummy config (useful in gitlab CI)
    config.load_kube_config(
        config_file=os.path.join(os.path.dirname(__file__), "dummy_config")
    )


@pytest.fixture
def ns_account_normal(tmp_path: Path) -> Generator[Account, None, None]:
    # a dedicated account to test the namespace resource
    # otherwise there could be clashes with other resources
    # that expect the namespace to be present, making it difficult
    # to test the namespace resource logic itself
    name = "test-account-ns"
    ns = f"tool-{name}"
    account = Account(
        namespace=ns,
        configmap_name=f"maintain-kubeusers-{name}",
        name=name,
        data_dir=f"{tmp_path}/data/project/{name}",
        uid=50005,
        is_admin=False,
        is_disabled=False,
    )

    try:
        client.CoreV1Api().delete_namespace(name=ns)
    except ApiException:
        pass

    yield account

    try:
        client.CoreV1Api().delete_namespace(name=ns)
    except ApiException:
        pass


def namespace_exists(ns: str) -> bool:
    core = client.CoreV1Api()
    try:
        core.read_namespace(name=ns)
    except ApiException:
        return False
    return True


def namespace_create(ns: str) -> None:
    core = client.CoreV1Api()
    core.create_namespace(
        body=client.V1Namespace(
            api_version="v1",
            kind="Namespace",
            metadata=client.V1ObjectMeta(
                name=ns,
            ),
        )
    )


def namespace_delete(ns: str) -> None:
    core = client.CoreV1Api()
    core.delete_namespace(ns)


@pytest.fixture
def account_normal(
    are_we_in_k8s: bool, tmp_path_factory: Any
) -> Generator[Account, None, None]:
    tmp_path = tmp_path_factory.mktemp("test-homes")
    name = "test-account-normal"
    ns = f"tool-{name}"
    account = Account(
        namespace=ns,
        configmap_name=f"maintain-kubeusers-{name}",
        name=name,
        data_dir=f"{tmp_path}/data/project/{name}",
        uid=50005,
        is_admin=False,
        is_disabled=False,
    )

    namespace_create(ns=ns)

    # wait until created
    if are_we_in_k8s:
        while not namespace_exists(ns=ns):
            sleep(0.5)

    yield account

    # teardown: delete ns
    namespace_delete(ns=ns)
    if are_we_in_k8s:
        while namespace_exists(ns=ns):
            sleep(0.5)


@pytest.fixture()
def ns_account_admin(tmp_path: Path) -> Account:
    # a dedicated account to test the namespace resource
    # otherwise there could be clashes with other resources
    # that expect the namespace to be present, making it difficult
    # to test the namespace resource logic itself
    name = "test-someadmin-ns"
    return Account(
        namespace="maintain-kubeusers",
        configmap_name=f"maintain-kubeusers-{name}",
        name=name,
        data_dir=f"{tmp_path}/data/project/{name}",
        uid=50005,
        is_admin=True,
        is_disabled=False,
    )


@pytest.fixture()
def account_admin(tmp_path: Path) -> Account:
    name = "test-someadmin"
    return Account(
        namespace="maintain-kubeusers",
        configmap_name=f"maintain-kubeusers-{name}",
        name=name,
        data_dir=f"{tmp_path}/data/project/{name}",
        uid=50006,
        is_admin=True,
        is_disabled=False,
    )


@pytest.fixture()
def account_normal_disabled(tmp_path: Path) -> Account:
    name = "test-account-disabled"
    return Account(
        namespace=f"tool-{name}",
        configmap_name=f"maintain-kubeusers-{name}",
        name=name,
        data_dir=f"{tmp_path}/data/project/{name}",
        uid=50005,
        is_admin=False,
        is_disabled=True,
    )


@pytest.fixture()
def account_admin_disabled(tmp_path: Path) -> Account:
    name = "test-some-disabled-admin"
    return Account(
        namespace="maintain-kubeusers",
        configmap_name=f"maintain-kubeusers-{name}",
        name=name,
        data_dir=f"{tmp_path}/data/project/{name}",
        uid=50006,
        is_admin=True,
        is_disabled=True,
    )


@pytest.fixture()
def quota_config() -> QuotaConfig:
    return QuotaConfig(
        default=QuotaEntry(
            version="test",
            cpu=8,
            memory=8,
            pod_cpu=3,
            pod_memory=6,
            pods=16,
            jobs=20,
            cronjobs=50,
            deployments=3,
            configmaps=10,
            secrets=64,
            services=1,
        ),
        overrides={},
    )


def pytest_addoption(parser: pytest.Parser) -> None:
    parser.addoption(
        "--in-k8s",
        action="store_true",
        default=False,
        help="For rebuilding cassettes inside k8s",
    )
    parser.addoption(
        "--try-local-kubeconfig",
        action="store_true",
        default=False,
        help="Try using local kube/config file",
    )


@pytest.fixture(scope="session")
def are_we_in_k8s(request: FixtureRequest) -> bool:
    return request.config.getoption("--in-k8s")  # type: ignore


@pytest.fixture(scope="session")
def try_local_kubeconfig(request: FixtureRequest) -> bool:
    return request.config.getoption("--try-local-kubeconfig")  # type: ignore


@pytest.fixture()
def ldap_conn() -> ldap3.Connection:
    server = ldap3.Server("ldap-server.example.com")
    return ldap3.Connection(
        server,
        read_only=True,
        user="cn=test,ou=testprofile,dc=wikimedia,dc=org",
        password="notreal",
        raise_exceptions=True,
        receive_timeout=60,
    )
