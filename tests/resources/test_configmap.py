import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.errors import ResourceError
from maintain_kubeusers.resources.configmap import Configmap


@pytest.fixture
def cm_normal_account(account_normal: Account) -> Configmap:
    cm = Configmap(account=account_normal)
    assert cm
    return cm


@pytest.mark.vcr
def test_cm_normal_account_needs_create_always_true_for_a_fresh_account(
    k8s_auth: None, cm_normal_account: Configmap
) -> None:
    assert cm_normal_account.needs_create(resource_data=None)


@pytest.mark.vcr
def test_cm_normal_account_do_create_works_fine_for_a_fresh_account(
    k8s_auth: None, cm_normal_account: Configmap
) -> None:
    new_resource_data = cm_normal_account.do_create(resource_data=None)
    assert new_resource_data == cm_normal_account.installed_message
    assert not cm_normal_account.needs_create(resource_data=new_resource_data)


@pytest.mark.vcr
def test_cm_normal_account_needs_update_never_true(
    k8s_auth: None, cm_normal_account: Configmap
) -> None:
    # we never update this resource
    assert not cm_normal_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_cm_normal_account_do_update_not_implemented(
    cm_normal_account: Configmap,
) -> None:
    with pytest.raises(NotImplementedError):
        cm_normal_account.do_update(resource_data=None)


@pytest.mark.vcr
def test_cm_normal_account_needs_delete_never_true(
    cm_normal_account: Configmap,
) -> None:
    # we never do this
    assert not cm_normal_account.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_cm_normal_account_do_delete_not_expected(
    cm_normal_account: Configmap,
) -> None:
    with pytest.raises(ResourceError):
        cm_normal_account.do_delete(resource_data=None)
