import os
from unittest.mock import Mock, patch

import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.resources.homedir import HomeDir


@pytest.fixture
def homedir_normal_account(account_normal: Account) -> HomeDir:
    homedir = HomeDir(account=account_normal)
    assert homedir
    return homedir


@pytest.mark.vcr
def test_homedir_normal_account_needs_create_always_true_if_no_resource_data(
    homedir_normal_account: HomeDir,
) -> None:
    assert homedir_normal_account.needs_create(resource_data=None)


@pytest.mark.vcr
def test_homedir_normal_account_needs_create_always_false_if_some_resource_data(
    homedir_normal_account: HomeDir,
) -> None:
    assert not homedir_normal_account.needs_create(resource_data="something")


@pytest.mark.vcr
def test_homedir_normal_account_do_create_works_fine_for_a_fresh_account(
    homedir_normal_account: HomeDir,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            homedir_normal_account.do_create(resource_data=None)
    assert os.path.exists(homedir_normal_account.account.data_dir)
    assert os.path.exists(f"{homedir_normal_account.account.data_dir}/logs")
    assert not os.path.exists(homedir_normal_account.disabled_flag)


@pytest.mark.vcr
def test_homedir_normal_account_needs_update_always_false_if_resource_data(
    homedir_normal_account: HomeDir,
) -> None:
    assert not homedir_normal_account.needs_update(resource_data="something")


@pytest.mark.vcr
def test_homedir_normal_account_needs_update_always_false_if_no_resource_data(
    homedir_normal_account: HomeDir,
) -> None:
    assert not homedir_normal_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_homedir_normal_account_do_update_not_implemented(
    homedir_normal_account: HomeDir,
) -> None:
    with pytest.raises(NotImplementedError):
        homedir_normal_account.do_update(resource_data=None)


@pytest.mark.vcr
def test_homedir_normal_account_needs_delete_never_true(
    homedir_normal_account: HomeDir,
) -> None:
    assert not homedir_normal_account.needs_delete(resource_data=None)


@pytest.fixture
def homedir_normal_account_disabled(
    account_normal_disabled: Account,
) -> HomeDir:
    homedir = HomeDir(account=account_normal_disabled)
    assert homedir
    return homedir


@pytest.mark.vcr
@patch("os.path.exists")
def test_homedir_normal_account_disabled_needs_delete_false_with_disabled_flag(
    mock_exists: Mock, homedir_normal_account_disabled: HomeDir
) -> None:
    mock_exists.side_effect = lambda filepath: {
        homedir_normal_account_disabled.home: False,
        homedir_normal_account_disabled.disabled_flag: True,
    }.get(filepath, False)
    assert not homedir_normal_account_disabled.needs_delete(
        resource_data="something"
    )


@pytest.mark.vcr
@patch("os.path.exists")
def test_homedir_normal_account_disabled_needs_delete_both_exist(
    mock_exists: Mock, homedir_normal_account_disabled: HomeDir
) -> None:
    mock_exists.side_effect = lambda filepath: {
        homedir_normal_account_disabled.home: True,
        homedir_normal_account_disabled.disabled_flag: True,
    }.get(filepath, False)
    assert not homedir_normal_account_disabled.needs_delete(
        resource_data="something"
    )


@pytest.mark.vcr
@patch("os.path.exists")
def test_homedir_normal_account_disabled_needs_delete_no_disabled(
    mock_exists: Mock, homedir_normal_account_disabled: HomeDir
) -> None:
    mock_exists.side_effect = lambda filepath: {
        homedir_normal_account_disabled.home: True,
        homedir_normal_account_disabled.disabled_flag: False,
    }.get(filepath, False)
    assert homedir_normal_account_disabled.needs_delete(
        resource_data="something"
    )


@pytest.mark.vcr
@patch("os.path.exists")
def test_homedir_normal_account_disabled_needs_delete_never_true_if_no_resource_data(
    mock_exists: Mock, homedir_normal_account_disabled: HomeDir
) -> None:
    mock_exists.side_effect = lambda filepath: {
        homedir_normal_account_disabled.home: True,
        homedir_normal_account_disabled.disabled_flag: False,
    }.get(filepath, False)
    assert not homedir_normal_account_disabled.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_homedir_normal_account_disabled_do_delete_disabled_flag_is_created(
    homedir_normal_account_disabled: HomeDir,
) -> None:
    # make sure home dir exists, disabled_flag is created
    os.makedirs(homedir_normal_account_disabled.account.data_dir, exist_ok=True)
    homedir_normal_account_disabled.do_delete(resource_data=None)
    assert os.path.exists(homedir_normal_account_disabled.disabled_flag)
