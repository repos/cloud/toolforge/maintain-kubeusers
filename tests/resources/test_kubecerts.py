import os
from datetime import UTC, datetime, timedelta
from functools import cache
from typing import Any
from unittest.mock import Mock, patch

import pytest
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509 import load_pem_x509_certificate
from cryptography.x509.oid import NameOID

from maintain_kubeusers.account import Account
from maintain_kubeusers.resources.kubecerts import (
    KUBECERT_RENEW_DAYS_AHEAD,
    KubeCerts,
)


@pytest.fixture
def kc_normal_account(monkeypatch: Any, account_normal: Account) -> KubeCerts:
    kc = KubeCerts(account=account_normal)

    # use a smaller pk for our tests (faster)
    @cache
    def _generate_small_pk() -> rsa.RSAPrivateKey:
        return rsa.generate_private_key(
            public_exponent=65537, key_size=512, backend=default_backend
        )

    monkeypatch.setattr(kc, "_generate_pk", _generate_small_pk)

    def _not_randomly_selected_for_renewal(expiry_time: datetime) -> bool:
        return False

    monkeypatch.setattr(
        kc,
        "_is_randomly_selected_for_renewal",
        _not_randomly_selected_for_renewal,
    )

    return kc


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_normal_account_needs_create_no_files(
    mock_isfile: Mock, kc_normal_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_normal_account.cert_file: False,
        kc_normal_account.key_file: False,
    }.get(filepath, False)
    assert kc_normal_account.needs_create(resource_data=None)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_normal_account_needs_create_no_key(
    mock_isfile: Mock, kc_normal_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_normal_account.cert_file: True,
        kc_normal_account.key_file: False,
    }.get(filepath, False)
    assert kc_normal_account.needs_create(resource_data=None)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_normal_account_needs_create_empty_data(
    mock_isfile: Mock, kc_normal_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_normal_account.cert_file: True,
        kc_normal_account.key_file: True,
    }.get(filepath, False)
    data: Any = {}
    assert kc_normal_account.needs_create(resource_data=data)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_normal_account_needs_create_empty_status(
    mock_isfile: Mock, kc_normal_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_normal_account.cert_file: True,
        kc_normal_account.key_file: True,
    }.get(filepath, False)
    data: Any = {
        "status": {},
    }
    assert kc_normal_account.needs_create(resource_data=data)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_normal_account_needs_create_negative(
    mock_isfile: Mock, kc_normal_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_normal_account.cert_file: True,
        kc_normal_account.key_file: True,
    }.get(filepath, False)
    data = {
        "status": {
            "expires": "2090",
        },
    }
    assert not kc_normal_account.needs_create(resource_data=data)


@pytest.mark.vcr
def test_kubecert_normal_account_do_create_verify_date(
    kc_normal_account: KubeCerts,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            new_resource_data = kc_normal_account.do_create(resource_data=None)

    # verify we stored a parseable date
    stored_expired_time = new_resource_data["status"]["expires"]
    expired_time = datetime.strptime(stored_expired_time, "%Y-%m-%dT%H:%M:%S")
    assert expired_time


@pytest.mark.vcr
def test_kubecert_normal_account_do_create_verify_certs(
    kc_normal_account: KubeCerts,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            kc_normal_account.do_create(resource_data=None)

    certfile = f"{kc_normal_account.account.data_dir}/.toolskube/client.crt"
    assert os.path.exists(certfile)
    keyfile = f"{kc_normal_account.account.data_dir}/.toolskube/client.key"
    assert os.path.exists(keyfile)

    with open(certfile, "rb") as cert_data:
        certificate = load_pem_x509_certificate(
            cert_data.read(), default_backend()
        )
        subject = certificate.subject
        cn = subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0]
        assert cn.value == "test-account-normal"
        assert cn.value == kc_normal_account.account.name


@pytest.mark.vcr
def test_kubecert_normal_account_needs_update_always_true_if_no_resource_data(
    kc_normal_account: KubeCerts,
) -> None:
    assert kc_normal_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_kubecert_normal_account_needs_update_empty_status(
    kc_normal_account: KubeCerts,
) -> None:
    data: Any = {
        "status": {},
    }
    assert kc_normal_account.needs_update(resource_data=data)


@pytest.mark.vcr
def test_kubecert_normal_account_needs_update_true_if_already_expired(
    kc_normal_account: KubeCerts,
) -> None:
    # expired 30 days ago
    expires_dt = datetime.now(UTC).replace(tzinfo=None) - timedelta(days=30)
    expires = expires_dt.replace(tzinfo=None).isoformat(timespec="seconds")
    data = {
        "status": {
            "expires": expires,
        },
    }
    assert kc_normal_account.needs_update(resource_data=data)


@pytest.mark.vcr
def test_kubecert_normal_account_needs_update_true_if_equal_renew_threshold(
    kc_normal_account: KubeCerts,
) -> None:
    days = KUBECERT_RENEW_DAYS_AHEAD
    expires_dt = datetime.now(UTC).replace(tzinfo=None) + timedelta(days=days)
    expires = expires_dt.replace(tzinfo=None).isoformat(timespec="seconds")
    data = {
        "status": {
            "expires": expires,
        },
    }
    assert kc_normal_account.needs_update(resource_data=data)


@pytest.mark.vcr
def test_kubecert_normal_account_needs_update_true_if_lt_renew_threshold(
    kc_normal_account: KubeCerts,
) -> None:
    expires_dt = datetime.now(UTC).replace(tzinfo=None) - timedelta(days=1)
    expires = expires_dt.replace(tzinfo=None).isoformat(timespec="seconds")
    data = {
        "status": {
            "expires": expires,
        },
    }
    assert kc_normal_account.needs_update(resource_data=data)


@pytest.mark.vcr
def test_kubecert_normal_account_needs_update_false_if_gt_renew_threshold(
    kc_normal_account: KubeCerts,
) -> None:
    days = KUBECERT_RENEW_DAYS_AHEAD + 1
    expires_dt = datetime.now(UTC).replace(tzinfo=None) + timedelta(days=days)
    expires = expires_dt.replace(tzinfo=None).isoformat(timespec="seconds")
    data = {
        "status": {
            "expires": expires,
        },
    }
    assert not kc_normal_account.needs_update(resource_data=data)


@pytest.mark.vcr
def test_kubecert_normal_account_do_update_works_fine_if_no_resource_data(
    kc_normal_account: KubeCerts,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            new_resource_data = kc_normal_account.do_update(resource_data=None)

    # verify we stored a parseable date
    stored_expire_time = new_resource_data["status"]["expires"]
    expire_time = datetime.strptime(stored_expire_time, "%Y-%m-%dT%H:%M:%S")
    assert expire_time


@pytest.fixture
def kc_admin_account(monkeypatch: Any, account_admin: Account) -> KubeCerts:
    kc = KubeCerts(account=account_admin)

    # use a smaller pk for our tests (faster)
    @cache
    def _generate_small_pk() -> rsa.RSAPrivateKey:
        return rsa.generate_private_key(
            public_exponent=65537, key_size=512, backend=default_backend
        )

    monkeypatch.setattr(kc, "_generate_pk", _generate_small_pk)

    def _not_randomly_selected_for_renewal() -> bool:
        return False

    monkeypatch.setattr(
        kc,
        "_is_randomly_selected_for_renewal",
        _not_randomly_selected_for_renewal,
    )

    return kc


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_admin_account_needs_create_is_true_if_no_cert_files_exists(
    mock_isfile: Mock, kc_admin_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_admin_account.cert_file: False,
        kc_admin_account.key_file: False,
    }.get(filepath, False)
    assert kc_admin_account.needs_create(resource_data=None)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_admin_account_needs_create_no_key(
    mock_isfile: Mock, kc_admin_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_admin_account.cert_file: True,
        kc_admin_account.key_file: False,
    }.get(filepath, False)
    assert kc_admin_account.needs_create(resource_data=None)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_admin_account_needs_create_no_data(
    mock_isfile: Mock, kc_admin_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_admin_account.cert_file: True,
        kc_admin_account.key_file: True,
    }.get(filepath, False)
    data: Any = None
    assert kc_admin_account.needs_create(resource_data=data)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_admin_account_needs_create_empty_data(
    mock_isfile: Mock, kc_admin_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_admin_account.cert_file: True,
        kc_admin_account.key_file: True,
    }.get(filepath, False)
    data: Any = {}
    assert kc_admin_account.needs_create(resource_data=data)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_admin_account_needs_create_empty_status(
    mock_isfile: Mock, kc_admin_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_admin_account.cert_file: True,
        kc_admin_account.key_file: True,
    }.get(filepath, False)
    data: Any = {
        "status": {},
    }
    assert kc_admin_account.needs_create(resource_data=data)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kubecert_admin_account_needs_create_negative(
    mock_isfile: Mock, kc_admin_account: KubeCerts
) -> None:
    mock_isfile.side_effect = lambda filepath: {
        kc_admin_account.cert_file: True,
        kc_admin_account.key_file: True,
    }.get(filepath, False)
    data = {
        "status": {
            "expires": "2090",
        },
    }
    assert not kc_admin_account.needs_create(resource_data=data)


@pytest.mark.vcr
def test_kubecert_admin_account_do_create_works_fine_if_no_resource_data(
    kc_admin_account: KubeCerts,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            new_resource_data = kc_admin_account.do_create(resource_data=None)

    # verify we stored a parseable date
    stored_expired_time = new_resource_data["status"]["expires"]
    expired_time = datetime.strptime(stored_expired_time, "%Y-%m-%dT%H:%M:%S")
    assert expired_time


@pytest.mark.vcr
def test_kubecert_admin_account_needs_update_always_true_if_no_resource_data(
    kc_admin_account: KubeCerts,
) -> None:
    assert kc_admin_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_kubecert_admin_account_needs_update_empty_status(
    kc_admin_account: KubeCerts,
) -> None:
    data: dict[str, Any] = {
        "status": None,
    }
    assert kc_admin_account.needs_update(resource_data=data)


@pytest.mark.vcr()
def test_kubecert_admin_account_do_update_works_fine_if_no_resource_data(
    kc_admin_account: KubeCerts,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            new_resource_data = kc_admin_account.do_update(resource_data=None)

    # verify we stored a parseable date
    stored_expired_time = new_resource_data["status"]["expires"]
    expired_time = datetime.strptime(stored_expired_time, "%Y-%m-%dT%H:%M:%S")
    assert expired_time


@pytest.mark.vcr
def test_kubecert_needs_delete_never_true(kc_normal_account: KubeCerts) -> None:
    # we never do this
    assert not kc_normal_account.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_kubecert_do_delete_not_implemented(
    kc_normal_account: KubeCerts,
) -> None:
    # we never do this
    with pytest.raises(NotImplementedError):
        kc_normal_account.do_delete(resource_data=None)
