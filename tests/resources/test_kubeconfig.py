import os
import pathlib
from unittest.mock import Mock, patch

import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.resources.kubeconfig import Kubeconfig


@pytest.fixture
def kc_normal_account(account_normal: Account) -> Kubeconfig:
    kc = Kubeconfig(
        account=account_normal,
        project="tools",
        api_server="localhost",
        ca_data="somecadata",
    )
    return kc


@pytest.mark.vcr
def test_kc_normal_account_needs_create_true_if_no_data(
    kc_normal_account: Kubeconfig,
) -> None:
    assert kc_normal_account.needs_create(resource_data=None)


@pytest.mark.vcr
def test_kc_normal_account_needs_create_fails_if_somedata(
    kc_normal_account: Kubeconfig,
) -> None:
    assert not kc_normal_account.needs_create(resource_data="something")


@pytest.mark.vcr
def test_kc_normal_account_do_create_works_fine_for_a_fresh_account(
    kc_normal_account: Kubeconfig,
) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            with patch("os.fchown", autospec=True):
                kc_normal_account.do_create(resource_data=None)

    assert os.path.exists(kc_normal_account.path)


@pytest.mark.vcr
def test_kc_normal_account_needs_update_always_false(
    kc_normal_account: Kubeconfig,
) -> None:
    assert not kc_normal_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_kc_normal_account_needs_update_always_false_even_with_data(
    kc_normal_account: Kubeconfig,
) -> None:
    assert not kc_normal_account.needs_update(resource_data="something")


@pytest.mark.vcr
def test_kc_normal_account_do_update_is_not_implemented(
    kc_normal_account: Kubeconfig,
) -> None:
    with pytest.raises(NotImplementedError):
        kc_normal_account.do_update(resource_data=None)


@pytest.mark.vcr
def test_kc_normal_account_needs_delete_always_false(
    kc_normal_account: Kubeconfig,
) -> None:
    assert not kc_normal_account.needs_delete(resource_data=None)


@pytest.fixture
def kc_normal_account_disabled(account_normal_disabled: Account) -> Kubeconfig:
    kc = Kubeconfig(
        account=account_normal_disabled,
        project="tools",
        api_server="localhost",
        ca_data="somecadata",
    )
    return kc


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kc_normal_account_disabled_needs_delete_file_exists_then_returns_true(
    mock_isfile: Mock,
    kc_normal_account_disabled: Kubeconfig,
) -> None:
    mock_isfile.side_effect = [True]
    assert kc_normal_account_disabled.needs_delete(resource_data=None)


@patch("os.path.isfile")
@pytest.mark.vcr
def test_kc_normal_account_disabled_needs_delete_file_dont_exists_then_returns_false(
    mock_isfile: Mock,
    kc_normal_account_disabled: Kubeconfig,
) -> None:
    mock_isfile.side_effect = [False]
    assert not kc_normal_account_disabled.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_kc_normal_account_disabled_do_delete_works_as_expected(
    kc_normal_account_disabled: Kubeconfig,
) -> None:
    # create stuff so it can be deleted by the function being tested
    os.makedirs(os.path.dirname(kc_normal_account_disabled.path))
    pathlib.Path(kc_normal_account_disabled.path).touch()

    kc_normal_account_disabled.do_delete(resource_data=None)
    assert not os.path.exists(kc_normal_account_disabled.path)
