import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.resources.kyverno_pod_policy import KyvernoPodPolicy


@pytest.fixture
def kpp_normal(account_normal: Account) -> KyvernoPodPolicy:
    kpp = KyvernoPodPolicy(account=account_normal)
    return kpp


@pytest.mark.vcr
def test_kpp_normal_generate_policy_works_as_expected(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    policy = kpp_normal._generate_policy()
    assert policy["metadata"]["namespace"] == kpp_normal.account.namespace


@pytest.mark.vcr
def test_kpp_normal_needs_create_always_true_if_no_resource_data(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    assert kpp_normal.needs_create(resource_data=None)


@pytest.mark.vcr
def test_kpp_normal_needs_create_always_false_if_some_resource_data(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    assert not kpp_normal.needs_create(resource_data="something")


@pytest.mark.vcr
def test_kpp_normal_do_create_returns_the_expected_resource_data(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    new_resource_data = kpp_normal.do_create(resource_data=None)
    assert new_resource_data == kpp_normal.desired_version


@pytest.mark.vcr
def test_kpp_normal_needs_update_always_false_if_expected_resource_data(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    assert not kpp_normal.needs_update(resource_data=kpp_normal.desired_version)


@pytest.mark.vcr
def test_kpp_normal_needs_update_always_true_if_unexpected_resource_data(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    assert kpp_normal.needs_update(
        resource_data="something-different-than-expected"
    )


@pytest.mark.vcr
def test_kpp_normal_do_update_returns_expected_resource_data(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    # in order to test the update, something should be present already
    # so run do_create first
    new_resource_data = kpp_normal.do_create(resource_data=None)
    assert new_resource_data == kpp_normal.desired_version

    new_resource_data = kpp_normal.do_update(resource_data=None)
    assert new_resource_data == kpp_normal.desired_version


@pytest.mark.vcr
def test_kpp_normal_needs_delete_always_false(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    assert not kpp_normal.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_kpp_normal_do_delete_not_implemented(
    kpp_normal: KyvernoPodPolicy,
) -> None:
    with pytest.raises(NotImplementedError):
        kpp_normal.do_delete(resource_data=None)


@pytest.fixture
def kpp_admin(account_admin: Account) -> KyvernoPodPolicy:
    kpp = KyvernoPodPolicy(
        account=account_admin,
    )
    return kpp


@pytest.mark.vcr
def test_kpp_admin_needs_create_always_false(
    kpp_admin: KyvernoPodPolicy,
) -> None:
    assert not kpp_admin.needs_create(resource_data=None)


@pytest.mark.vcr
def test_kpp_admin_needs_update_always_false(
    kpp_admin: KyvernoPodPolicy,
) -> None:
    assert not kpp_admin.needs_update(resource_data=None)


@pytest.mark.vcr
def test_kpp_admin_needs_delete_always_false(
    kpp_admin: KyvernoPodPolicy,
) -> None:
    assert not kpp_admin.needs_delete(resource_data=None)
