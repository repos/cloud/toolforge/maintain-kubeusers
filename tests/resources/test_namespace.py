import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.resources.namespace import Namespace


@pytest.fixture
def ns_normal_account(ns_account_normal: Account) -> Namespace:
    namespace = Namespace(account=ns_account_normal)
    assert namespace
    return namespace


@pytest.mark.vcr
def test_ns_normal_account_needs_create_always_true_for_a_fresh_account(
    ns_normal_account: Namespace,
) -> None:
    assert ns_normal_account.needs_create(resource_data=None)


@pytest.mark.vcr
def test_ns_normal_account_do_create_works_as_expected(
    k8s_auth: None, ns_normal_account: Namespace
) -> None:
    new_resource_data = ns_normal_account.do_create(resource_data=None)
    assert new_resource_data
    assert ns_normal_account.needs_create(resource_data=None)


@pytest.mark.vcr
def test_ns_normal_account_needs_update_never_true(
    ns_normal_account: Namespace,
) -> None:
    # we never update this resource
    assert not ns_normal_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_ns_normal_account_do_update_not_implemented(
    ns_normal_account: Namespace,
) -> None:
    with pytest.raises(NotImplementedError):
        ns_normal_account.do_update(resource_data=None)


@pytest.mark.vcr
def test_ns_normal_account_needs_delete_never_true(
    ns_normal_account: Namespace,
) -> None:
    # this account is not disabled
    assert not ns_normal_account.needs_delete(resource_data=None)


@pytest.fixture
def ns_normal_account_disabled(account_normal_disabled: Account) -> Namespace:
    namespace = Namespace(account=account_normal_disabled)
    assert namespace
    return namespace


@pytest.mark.vcr
def test_ns_normal_account_disabled_needs_delete_somedata_always_true(
    ns_normal_account_disabled: Namespace,
) -> None:
    assert ns_normal_account_disabled.needs_delete(resource_data="something")


@pytest.mark.vcr
def test_ns_normal_account_disabled_needs_delete_nodata_always_false(
    ns_normal_account_disabled: Namespace,
) -> None:
    assert not ns_normal_account_disabled.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_ns_normal_account_disabled_do_delete_works_fine(
    k8s_auth: None, ns_normal_account_disabled: Namespace
) -> None:
    ns_normal_account_disabled.do_delete(resource_data=None)


@pytest.fixture
def ns_admin_account(ns_account_admin: Account) -> Namespace:
    namespace = Namespace(account=ns_account_admin)
    assert namespace
    return namespace


@pytest.mark.vcr
def test_ns_admin_account_needs_create_never_true(
    ns_admin_account: Namespace,
) -> None:
    assert not ns_admin_account.needs_create(resource_data=None)


@pytest.mark.vcr
def test_ns_admin_account_needs_update_never_true(
    ns_admin_account: Namespace,
) -> None:
    assert not ns_admin_account.needs_update(resource_data=None)


@pytest.mark.vcr
def test_ns_admin_account_needs_delete_somedata_never_true(
    ns_admin_account: Namespace,
) -> None:
    assert not ns_admin_account.needs_delete(resource_data="something")


@pytest.mark.vcr
def test_ns_admin_account_needs_delete_nodata_never_true(
    ns_admin_account: Namespace,
) -> None:
    assert not ns_admin_account.needs_delete(resource_data=None)
