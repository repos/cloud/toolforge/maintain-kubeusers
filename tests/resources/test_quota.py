import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.quota import QuotaConfig
from maintain_kubeusers.resources.quota import Quota


@pytest.fixture
def quota_normal(account_normal: Account, quota_config: QuotaConfig) -> Quota:
    quota = Quota(
        account=account_normal,
        quotaentry=quota_config.get_for_user(account_normal.name),
    )
    assert quota
    return quota


@pytest.mark.vcr
def test_quota_normal_needs_create_always_true_for_a_fresh_account(
    quota_normal: Quota,
) -> None:
    assert quota_normal.needs_create(resource_data=None)


@pytest.mark.vcr
def test_quota_normal_needs_create_always_false_if_somedata(
    quota_normal: Quota,
) -> None:
    assert not quota_normal.needs_create(resource_data={"something"})


@pytest.mark.vcr
def test_quota_normal_do_create_works_fine(quota_normal: Quota) -> None:
    new_resource_data = quota_normal.do_create(resource_data=None)
    assert new_resource_data == quota_normal.quotaentry.version


@pytest.mark.vcr
def test_quota_normal_needs_update_always_false_if_expected_version(
    quota_normal: Quota,
) -> None:
    assert not quota_normal.needs_update(
        resource_data=quota_normal.quotaentry.version
    )


@pytest.mark.vcr
def test_quota_normal_needs_update_always_true_if_unexpected_version(
    quota_normal: Quota,
) -> None:
    assert quota_normal.needs_update(
        resource_data="something-different-than-expected"
    )


@pytest.mark.vcr
def test_quota_normal_do_update_works_fine(quota_normal: Quota) -> None:
    # in order to test the update, something should be present already
    new_resource_data = quota_normal.do_create(resource_data=None)
    assert new_resource_data == quota_normal.quotaentry.version

    new_resource_data = quota_normal.do_update(resource_data=None)
    assert new_resource_data == quota_normal.quotaentry.version


@pytest.mark.vcr
def test_quota_normal_needs_delete_always_false(quota_normal: Quota) -> None:
    assert not quota_normal.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_quota_normal_do_delete_not_implemented(quota_normal: Quota) -> None:
    with pytest.raises(NotImplementedError):
        quota_normal.do_delete(resource_data=None)


@pytest.fixture
def quota_admin(account_admin: Account, quota_config: QuotaConfig) -> Quota:
    quota = Quota(
        account=account_admin,
        quotaentry=quota_config.get_for_user(account_admin.name),
    )
    assert quota
    return quota


@pytest.mark.vcr
def test_quota_admin_needs_create_always_false(quota_admin: Quota) -> None:
    assert not quota_admin.needs_create(resource_data=None)


@pytest.mark.vcr
def test_quota_admin_needs_update_always_false(quota_admin: Quota) -> None:
    assert not quota_admin.needs_update(resource_data=None)


@pytest.mark.vcr
def test_quota_admin_needs_delete_always_false(quota_admin: Quota) -> None:
    assert not quota_admin.needs_delete(resource_data=None)
