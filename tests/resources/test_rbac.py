import pytest

from maintain_kubeusers.account import Account
from maintain_kubeusers.resources.rbac import RBAC


@pytest.fixture
def rbac_normal(account_normal: Account) -> RBAC:
    rbac = RBAC(account=account_normal)
    return rbac


@pytest.mark.vcr
def test_rbac_normal_needs_create_always_true_for_a_fresh_account(
    rbac_normal: RBAC,
) -> None:
    assert rbac_normal.needs_create(resource_data=None)


@pytest.mark.vcr
def test_rbac_normal_needs_create_always_false_if_resource_data(
    rbac_normal: RBAC,
) -> None:
    assert not rbac_normal.needs_create(resource_data="something")


@pytest.mark.vcr
def test_rbac_normal_do_create_works_fine(
    k8s_auth: None, rbac_normal: RBAC
) -> None:
    new_resource_data = rbac_normal.do_create(resource_data=None)
    assert new_resource_data == rbac_normal.installed_message
    assert not rbac_normal.needs_create(
        resource_data=rbac_normal.installed_message
    )


@pytest.mark.vcr
def test_rbac_normal_needs_update_always_false(rbac_normal: RBAC) -> None:
    assert not rbac_normal.needs_update(resource_data=None)


@pytest.mark.vcr
def test_rbac_normal_do_update_not_implemented(rbac_normal: RBAC) -> None:
    with pytest.raises(NotImplementedError):
        rbac_normal.do_update(resource_data=None)


@pytest.mark.vcr
def test_rbac_normal_needs_delete_always_false(rbac_normal: RBAC) -> None:
    assert not rbac_normal.needs_update(resource_data=None)


@pytest.fixture
def rbac_admin(account_admin: Account) -> RBAC:
    rbac = RBAC(account=account_admin)
    return rbac


@pytest.mark.vcr
def test_rbac_admin_needs_create_always_true_if_nodata(
    rbac_admin: RBAC,
) -> None:
    assert rbac_admin.needs_create(resource_data=None)


@pytest.mark.vcr
def test_rbac_admin_needs_create_always_false_if_somedata(
    rbac_admin: RBAC,
) -> None:
    assert not rbac_admin.needs_create(resource_data="something")


@pytest.mark.vcr
def test_rbac_admin_do_create_works_fine(
    k8s_auth: None, rbac_admin: RBAC
) -> None:
    new_resource_data = rbac_admin.do_create(resource_data=None)
    assert new_resource_data == rbac_admin.installed_message


@pytest.mark.vcr
def test_rbac_admin_needs_update_always_false(rbac_admin: RBAC) -> None:
    assert not rbac_admin.needs_update(resource_data=None)


@pytest.mark.vcr
def test_rbac_admin_do_update_not_implemented(rbac_admin: RBAC) -> None:
    with pytest.raises(NotImplementedError):
        rbac_admin.do_update(resource_data=None)


@pytest.mark.vcr
def test_rbac_admin_needs_delete_always_false(rbac_admin: RBAC) -> None:
    assert not rbac_admin.needs_delete(resource_data=None)


@pytest.fixture
def rbac_disabled_admin(account_admin_disabled: Account) -> RBAC:
    rbac = RBAC(account=account_admin_disabled)
    return rbac


@pytest.mark.vcr
def test_rbac_disabled_admin_needs_delete_always_true(
    rbac_disabled_admin: RBAC,
) -> None:
    assert rbac_disabled_admin.needs_delete(resource_data=None)


@pytest.mark.vcr
def test_rbac_disabled_admin_do_delete_works_fine(
    k8s_auth: None, rbac_disabled_admin: RBAC
) -> None:
    rbac_disabled_admin.do_delete(resource_data=None)
