import os
from pathlib import Path
from time import sleep, time
from typing import Any, Generator
from unittest.mock import patch

import pytest
import yaml
from kubernetes import client
from kubernetes.client.rest import ApiException

from maintain_kubeusers.account import Account
from maintain_kubeusers.quota import QuotaConfig
from maintain_kubeusers.resources.base import BaseResource
from maintain_kubeusers.user import User


def user_reconcile(user: User) -> None:
    with patch("os.chmod", autospec=True):
        with patch("os.chown", autospec=True):
            with patch("os.fchown", autospec=True):
                user.reconcile()


def get_configmap_data(ns: str, cm: str, key: str) -> Any:
    core = client.CoreV1Api()
    resp = core.read_namespaced_config_map(namespace=ns, name=cm)
    return yaml.safe_load(resp.data[key])


def assert_configmap_data(ns: str, cm: str, key: str, value: Any) -> None:
    resource_data = get_configmap_data(ns=ns, cm=cm, key=key)
    assert resource_data == value


def assert_configmap_no_data(ns: str, cm: str, key: str) -> None:
    try:
        get_configmap_data(ns=ns, cm=cm, key=key)
    except KeyError:
        # expected!
        return

    assert False


@pytest.fixture
def user_normal(
    monkeypatch: Any, tmp_path: Path, quota_config: QuotaConfig
) -> Generator[User, None, None]:
    name = "reconcile-test"
    project = "tools"
    home = f"{tmp_path}/data/project/{name}"
    uid = 54321
    user = User(
        name=name,
        uid=uid,
        home=home,
        project=project,
        pwdAccountLockedTime=None,
        pwdPolicySubentry=None,
        quota=quota_config.get_for_user(name),
        api_server="someapiserver",
        ca_data="somecadata",
    )
    # cleanup potential pre-existing ns in case of a run previous to this one
    core = client.CoreV1Api()
    try:
        core.delete_namespace(name=user.account.namespace)
    except Exception:
        pass

    yield user

    try:
        core.delete_namespace(name=user.account.namespace)
    except Exception:
        pass


@pytest.mark.vcr
def test_resources_reconcile_user_normal_works_as_expected(
    user_normal: User,
) -> None:
    user_reconcile(user_normal)

    core = client.CoreV1Api()

    # verify namespace was created
    ns = user_normal.account.namespace
    resp = core.read_namespace(ns)
    assert resp.metadata.name == ns

    # verify configmap was created
    cm = user_normal.account.configmap_name
    resp = core.read_namespaced_config_map(namespace=ns, name=cm)
    assert resp.metadata.name == cm

    # verify homedir was created
    assert os.path.exists(user_normal.account.data_dir)

    # verify kubeconfig was created
    assert os.path.isfile(f"{user_normal.account.data_dir}/.kube/config")
    assert_configmap_data(ns=ns, cm=cm, key="kubeconfig", value="installed")

    # verify certs were created
    assert os.path.isfile(
        f"{user_normal.account.data_dir}/.toolskube/client.crt"
    )
    assert os.path.isfile(
        f"{user_normal.account.data_dir}/.toolskube/client.key"
    )

    resource_data = get_configmap_data(ns=ns, cm=cm, key="kubecerts")
    assert resource_data["status"]["expires"]

    # verify RBAC was created
    rbac = client.RbacAuthorizationV1Api()
    name = f"{user_normal.account.name}-tool-binding"
    resp = rbac.read_namespaced_role_binding(namespace=ns, name=name)
    assert resp.metadata.name == name

    # hardcoded in the resource class
    assert_configmap_data(ns=ns, cm=cm, key="rbac", value="installed")

    # verify quota
    resp = core.read_namespaced_resource_quota(
        namespace=ns, name=user_normal.account.namespace
    )
    assert resp.metadata.name == user_normal.account.namespace

    resp = core.read_namespaced_limit_range(
        namespace=ns, name=user_normal.account.namespace
    )
    assert resp.metadata.name == user_normal.account.namespace

    # version from the fixture
    assert_configmap_data(ns=ns, cm=cm, key="quota_version", value="test")

    # verify Kyverno policy
    api = client.CustomObjectsApi()
    name = "toolforge-kyverno-pod-policy"
    resp = api.get_namespaced_custom_object(
        group="kyverno.io",
        version="v1",
        namespace=ns,
        plural="policies",
        name=name,
    )
    assert resp["metadata"]["name"] == name


@pytest.mark.vcr
def test_resources_reconcile_user_normal_update_works_as_expected(
    k8s_auth: None, user_normal: User
) -> None:
    # first, create normal user resources so we can trigger the update path later
    user_reconcile(user_normal)

    ns = user_normal.account.namespace
    cm = user_normal.account.configmap_name
    key = "quota_version"
    data = "something-unexpected-to-force-an-update"

    # set the configmap in a way that will force an update
    # as of today, we only really update quotas
    core = client.CoreV1Api()
    core.patch_namespaced_config_map(
        namespace=ns,
        name=cm,
        body=client.V1ConfigMap(
            api_version="v1",
            kind="ConfigMap",
            metadata=client.V1ObjectMeta(name=cm),
            data={key: data},
        ),
    )

    assert_configmap_data(ns=ns, cm=cm, key=key, value=data)

    # this should trigger the update path
    user_reconcile(user_normal)

    # default version from the fixture
    assert_configmap_data(ns=ns, cm=cm, key=key, value="test")


@pytest.fixture
def user_normal2(
    monkeypatch: Any, tmp_path: Path, quota_config: QuotaConfig
) -> Generator[User, None, None]:
    name = "reconcile-test"
    project = "tools"
    home = f"{tmp_path}/data/project/{name}"
    uid = 54321
    user = User(
        name=name,
        uid=uid,
        home=home,
        project=project,
        pwdAccountLockedTime=None,
        pwdPolicySubentry=None,
        quota=quota_config.get_for_user(name),
        api_server="someapiserver",
        ca_data="somecadata",
    )
    # cleanup potential pre-existing ns in case of a run previous to this one
    core = client.CoreV1Api()
    try:
        core.delete_namespace(name=user.account.namespace)
    except Exception:
        pass

    yield user

    try:
        core.delete_namespace(name=user.account.namespace)
    except Exception:
        pass


@pytest.mark.vcr
def test_resources_reconcile_user_normal_works_for_resource_deletion(
    user_normal2: User,
) -> None:
    # this test case verifies that a normal active account (not disabled, not admin)
    # can have a resource deleted as part of the reconcile loop
    class FakeResource(BaseResource):
        CONFIGMAP_KEY: str = "fake_resource"

        def __init__(self, account: Account) -> None:
            self.account = account
            self.resource_data = "something"

        def needs_create(self, resource_data: Any) -> bool:
            return True if not resource_data else False

        def do_create(self, resource_data: Any) -> Any:
            return self.resource_data

        def needs_update(self, resource_data: Any) -> bool:
            return False

        def do_update(self, resource_data: Any) -> Any:
            pass

        def needs_delete(self, resource_data: Any) -> bool:
            return True if resource_data else False

        def do_delete(self, resource_data: Any) -> None:
            pass

    user_normal2.resources.resources.append(FakeResource(user_normal2.account))
    ns = user_normal2.account.namespace
    cm = user_normal2.account.configmap_name

    # per the FakeResource logic, this first reconcile() creates the resource
    user_reconcile(user_normal2)
    assert_configmap_data(
        ns=ns, cm=cm, key=FakeResource.CONFIGMAP_KEY, value="something"
    )

    # per the FakeResource logic, this second reconcile() deletes the resource
    user_reconcile(user_normal2)
    assert_configmap_no_data(ns=ns, cm=cm, key=FakeResource.CONFIGMAP_KEY)


@pytest.fixture
def user_disabled(
    monkeypatch: Any, tmp_path: Path, quota_config: QuotaConfig
) -> User:
    name = "reconcile-disabled-test"
    project = "tools"
    home = f"{tmp_path}/data/project/{name}"
    uid = 54322
    user = User(
        name=name,
        uid=uid,
        home=home,
        project=project,
        pwdAccountLockedTime="yes-something",
        pwdPolicySubentry="yes-something",
        quota=quota_config.get_for_user(name),
        api_server="someapiserver",
        ca_data="somecadata",
    )
    assert user
    return user


@pytest.mark.vcr
def test_resources_reconcile_user_disabled_works_as_expected(
    k8s_auth: None, user_disabled: User
) -> None:
    core = client.CoreV1Api()
    ns = f"tool-{user_disabled.account.name}"

    # create namespace so we can check later that it is deleted
    try:
        core.create_namespace(
            body=client.V1Namespace(
                api_version="v1",
                kind="Namespace",
                metadata=client.V1ObjectMeta(
                    name=ns,
                ),
            )
        )
    except ApiException as api_ex:
        if api_ex.status == 409 and "AlreadyExists" in api_ex.body:
            pass
        else:
            raise

    # create configmap to store some resource data
    try:
        core.create_namespaced_config_map(
            namespace=ns,
            body=client.V1ConfigMap(
                api_version="v1",
                kind="ConfigMap",
                metadata=client.V1ObjectMeta(
                    name=user_disabled.account.configmap_name
                ),
                data={"namespace": "created", "homedir": "installed"},
            ),
        )
    except ApiException as api_ex:
        if api_ex.status == 409 and "AlreadyExists" in api_ex.body:
            pass
        else:
            raise

    # create the home dir so we can check the disabled flag is created
    os.makedirs(user_disabled.account.data_dir, exist_ok=True)

    user_reconcile(user_disabled)

    assert not os.path.isfile(f"{user_disabled.account.data_dir}/.kube/config")
    assert os.path.isfile(f"{user_disabled.account.data_dir}/k8s.disabled")

    # wait for ns to terminate
    try:
        max_time = time() + 60  # 60 seconds from now
        while time() < max_time:
            core.read_namespace(name=ns)
            sleep(0.5)
        assert not "namespace not deleted by reconcile()"
    except ApiException:
        pass


@pytest.fixture
def user_admin(monkeypatch: Any, tmp_path: Path) -> Generator[User, None, None]:
    name = "admin-reconcile-test"
    project = "tools"
    home = f"{tmp_path}/data/project/{name}"
    uid = 54322
    user = User(
        name=name,
        uid=uid,
        home=home,
        project=project,
        pwdAccountLockedTime=None,
        pwdPolicySubentry=None,
        quota=None,
        api_server="someapiserver",
        ca_data="somecadata",
        admin=True,
    )

    # cleanup potential pre-existing configmap in case of a run previous to this one
    # we can't simply delete the namespace because is an admin account
    core = core = client.CoreV1Api()
    try:
        core.delete_namespaced_config_map(
            name=user.account.configmap_name, namespace=user.account.namespace
        )
    except Exception:
        pass

    yield user

    try:
        core.delete_namespaced_config_map(
            name=user.account.configmap_name, namespace=user.account.namespace
        )
    except Exception:
        pass


@pytest.mark.vcr
def test_resources_reconcile_user_admin_works_as_expected(
    k8s_auth: None, user_admin: User
) -> None:
    user_reconcile(user_admin)

    core = client.CoreV1Api()
    ns = user_admin.account.namespace

    # verify configmap was created
    cm = user_admin.account.configmap_name
    resp = core.read_namespaced_config_map(namespace=ns, name=cm)
    assert resp.metadata.name == cm

    # verify homedir was created
    assert os.path.exists(user_admin.account.data_dir)

    # verify kubeconfig was created
    assert os.path.isfile(f"{user_admin.account.data_dir}/.kube/config")
    assert_configmap_data(ns=ns, cm=cm, key="kubeconfig", value="installed")

    # verify certs were created
    assert os.path.isfile(f"{user_admin.account.data_dir}/.admkube/client.crt")
    assert os.path.isfile(f"{user_admin.account.data_dir}/.admkube/client.key")

    resource_data = get_configmap_data(ns=ns, cm=cm, key="kubecerts")
    assert resource_data["status"]["expires"]

    # verify RBAC was created
    rbac = client.RbacAuthorizationV1Api()
    name = f"{user_admin.account.name}-view-binding"
    resp = rbac.read_cluster_role_binding(name=name)
    assert resp.metadata.name == name

    name = f"{user_admin.account.name}-binding"
    resp = rbac.read_cluster_role_binding(name=name)
    assert resp.metadata.name == name

    # NOTE: hardcoded in the resource class
    assert_configmap_data(ns=ns, cm=cm, key="rbac", value="installed")


@pytest.fixture
def user_admin_disabled(k8s_auth: None, tmp_path: Path) -> User:
    name = "admin-reconcile-disabled-test"
    project = "tools"
    home = f"{tmp_path}/data/project/{name}"
    uid = 54322
    user = User(
        name=name,
        uid=uid,
        home=home,
        project=project,
        pwdAccountLockedTime="yes-something",
        pwdPolicySubentry="yes-something",
        quota=None,
        api_server="someapiserver",
        ca_data="somecadata",
        admin=True,
    )
    # create the home dir so we can check later disabled flag is created
    os.makedirs(user.account.data_dir, exist_ok=True)

    # create configmap to store some resource data
    core = client.CoreV1Api()
    core.create_namespaced_config_map(
        namespace=user.account.namespace,
        body=client.V1ConfigMap(
            api_version="v1",
            kind="ConfigMap",
            metadata=client.V1ObjectMeta(name=user.account.configmap_name),
            data={"namespace": "created", "homedir": "installed"},
        ),
    )

    return user


@pytest.mark.vcr
def test_resources_reconcile_user_admin_disabled_works_as_expected(
    k8s_auth: None, user_admin_disabled: User
) -> None:
    ns = user_admin_disabled.account.namespace
    cm = user_admin_disabled.account.configmap_name

    user_reconcile(user_admin_disabled)

    assert not os.path.isfile(
        f"{user_admin_disabled.account.data_dir}/.kube/config"
    )
    assert os.path.isfile(
        f"{user_admin_disabled.account.data_dir}/k8s.disabled"
    )

    # verify configmap was deleted
    with pytest.raises(ApiException):
        get_configmap_data(ns=ns, cm=cm, key="whatever")

    # verify RBAC was deleted
    rbac = client.RbacAuthorizationV1Api()
    name = f"{user_admin_disabled.account.name}-view-binding"

    with pytest.raises(ApiException):
        rbac.read_cluster_role_binding(name=name)

    name = f"{user_admin_disabled.account.name}-binding"
    with pytest.raises(ApiException):
        rbac.read_cluster_role_binding(name=name)
