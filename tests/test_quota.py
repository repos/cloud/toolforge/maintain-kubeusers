from pathlib import Path

import pytest

from maintain_kubeusers.quota import (
    QuotaConfig,
    QuotaEntry,
    parse_quota_config_file,
)


@pytest.fixture()
def quota_config() -> QuotaConfig:
    config = Path(__file__).parent / "fixtures" / "quota.yaml"
    return parse_quota_config_file(config)


@pytest.fixture()
def default_quota() -> QuotaEntry:
    return QuotaEntry(
        version="test",
        cpu=8,
        memory=8,
        pod_cpu=3,
        pod_memory=6,
        pods=16,
        jobs=20,
        cronjobs=50,
        deployments=3,
        configmaps=10,
        secrets=64,
        services=1,
    )


def test_QuotaEntry_override(default_quota: QuotaEntry) -> None:
    new_entry = default_quota.override(version="new", pods=160)
    assert new_entry.version == "test-new"
    assert new_entry.cpu == default_quota.cpu
    assert new_entry.pods == 160


def test_QuotaEntry_override_noversion(default_quota: QuotaEntry) -> None:
    with pytest.raises(ValueError) as raised:
        assert default_quota.override(pods=123) is None
    assert raised.value.args[0] == "Quota overrides must override version"


def test_load_default(
    quota_config: QuotaConfig, default_quota: QuotaEntry
) -> None:
    assert quota_config.default == default_quota


def test_load_for_non_overridden_tool(
    quota_config: QuotaConfig, default_quota: QuotaEntry
) -> None:
    quota = quota_config.get_for_user("test")
    assert quota == default_quota
    assert quota.version == "test"


def test_load_override(
    quota_config: QuotaConfig, default_quota: QuotaEntry
) -> None:
    quota = quota_config.get_for_user("lotsofjobs")
    assert quota.version == "test-lotsofjobs-1"
    assert quota.cpu == default_quota.cpu
    assert quota.cronjobs == 500
