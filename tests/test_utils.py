import re

import ldap3
from pytest import MonkeyPatch

from maintain_kubeusers.quota import QuotaConfig
from maintain_kubeusers.utils import get_admins_from_ldap, get_tools_from_ldap


def test_tools_search(
    monkeypatch: MonkeyPatch,
    ldap_conn: ldap3.Connection,
    quota_config: QuotaConfig,
) -> None:
    def mock_pg_search(*args, **kwargs):  # type: ignore[no-untyped-def]
        groups = [
            {
                "attributes": {
                    "cn": ["tools.example"],
                    "uidNumber": 50020,
                    "homeDirectory": "/data/project/example",
                }
            }
        ]
        for group in groups:
            yield group

    monkeypatch.setattr(
        ldap_conn.extend.standard, "paged_search", mock_pg_search
    )
    tools = get_tools_from_ldap(
        conn=ldap_conn,
        projectname="tools",
        api_server="someapiserver",
        ca_data="somecadata",
        quota=quota_config,
    )
    assert tools["example"].account.uid == 50020
    assert tools["example"].account.name == "example"
    assert tools["example"].account.data_dir == "/data/project/example"
    assert tools["example"].quota == quota_config.get_for_user("example")


def test_disabled_tool_search(
    monkeypatch: MonkeyPatch,
    ldap_conn: ldap3.Connection,
    quota_config: QuotaConfig,
) -> None:
    def mock_pg_search(*args, **kwargs):  # type: ignore[no-untyped-def]
        groups = [
            {
                "attributes": {
                    "cn": ["tools.example"],
                    "uidNumber": 50020,
                    "homeDirectory": "/data/project/example",
                    "pwdAccountLockedTime": "20240528145500.939815Z",
                    "pwdPolicySubentry": "cn=disabled,ou=ppolicies,dc=wikimedia,dc=org",
                }
            }
        ]
        for group in groups:
            yield group

    monkeypatch.setattr(
        ldap_conn.extend.standard, "paged_search", mock_pg_search
    )
    tools = get_tools_from_ldap(
        conn=ldap_conn,
        projectname="tools",
        api_server="someapiserver",
        ca_data="somecadata",
        quota=quota_config,
    )
    assert tools["example"].account.is_disabled


def test_admin_search(
    monkeypatch: MonkeyPatch, ldap_conn: ldap3.Connection
) -> None:
    def mock_pg_search(*args, **kwargs):  # type: ignore[no-untyped-def]
        if re.search(r"objectClass\=posixGroup", args[1]):
            members = [
                "uid=testy,ou=people,dc=wikimedia,dc=org",
                "uid=larry,ou=people,dc=wikimedia,dc=org",
                "uid=curly,ou=people,dc=wikimedia,dc=org",
                "uid=moe,ou=people,dc=wikimedia,dc=org",
                "uid=shemp,ou=people,dc=wikimedia,dc=org",
            ]
            groups = [{"attributes": {"member": (mem for mem in members)}}]
            for group in groups:
                yield group

            return

        # Escaping all those parens in python sucks
        early_str = re.escape("(&(objectClass=posixAccount)(")
        regex_str = early_str + r"([\w=]+).*"
        mem_uid_raw = re.sub(regex_str, r"\1", args[1])
        mem_uid = mem_uid_raw.split("=")[1]
        member = {
            "attributes": {
                "uid": [mem_uid],
                # Generate a predicatable UID from the name :-p
                "uidNumber": sum([ord(y) for y in list(mem_uid)]),
                "homeDirectory": "/home/{}".format(mem_uid),
            }
        }

        yield member

    monkeypatch.setattr(
        ldap_conn.extend.standard, "paged_search", mock_pg_search
    )

    admins = get_admins_from_ldap(
        conn=ldap_conn,
        projectname="tools",
        api_server="someapiserver",
        ca_data="somecadata",
    )
    assert admins["shemp"].account.uid == sum([ord(y) for y in list("shemp")])
    assert admins["shemp"].account.name == "shemp"
    assert admins["shemp"].account.data_dir == "/home/shemp"
    assert admins["shemp"].account.is_admin
